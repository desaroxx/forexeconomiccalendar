package ch.si.forexeconomiccalendar.app.events.entities;

import java.io.Serializable;

/**
 * Created by kensteiner on 19/07/14.
 */
public class TimeEntity implements Serializable {
    private int hour;
    private int minutes;

    public TimeEntity(int hour, int minutes) {
        this.hour = hour;
        this.minutes = minutes;
    }

    public int getHour() {
        return hour;
    }

    public int getMinutes() {
        return minutes;
    }
}
