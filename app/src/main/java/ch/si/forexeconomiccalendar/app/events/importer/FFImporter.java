package ch.si.forexeconomiccalendar.app.events.importer;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import ch.si.forexeconomiccalendar.app.exceptions.NetworkDownException;
import ch.si.forexeconomiccalendar.app.events.enums.*;
import ch.si.forexeconomiccalendar.app.util.Week;


/**
 * Created by kensteiner on 17/07/14.
 */
public class FFImporter {
    private static String SERVER_URL = "http://www.forexfactory.com";

    public static Document getEventsFromServer(Week week) throws NetworkDownException {
        Log.d("FFImporter", "getEventsFromServer");
        String weekString = "";
        switch (week) {
            case LAST_WEEK:
                weekString = "last";
                break;
            case THIS_WEEK:
                weekString = "this";
                break;
            case NEXT_WEEK:
                weekString = "next";
                break;
        }

        String computedServerURL = SERVER_URL + "/calendar.php?week=" + weekString;
        Log.d("FFImporter", "getEventsFromServer from: " + computedServerURL);
        Document doc = null;
        long startTime = System.currentTimeMillis();
        try {
            doc = Jsoup.connect(computedServerURL).get();
        } catch (IOException e) {
            e.printStackTrace();
            throw new NetworkDownException();
        }
        long duration = System.currentTimeMillis() - startTime;
        Log.d("FFImporter", "list data retrieval duration in ms: " + duration);
        return doc;
    }

    public static Document getDetailsFromServer(int id) throws NetworkDownException {
        Log.d("FFImporter", "getDetailsFromServer");

        String computedServerURL = SERVER_URL + "/flex.php?do=ajax&contentType=Content&flex=calendar_mainCal&details=" + id;
        Document doc = null;
        long startTime = System.currentTimeMillis();
        try {
            doc = Jsoup.connect(computedServerURL).get();
            doc = Jsoup.parse(doc.text());
        } catch (IOException e) {
            e.printStackTrace();
            throw new NetworkDownException();
        }
        long duration = System.currentTimeMillis() - startTime;
        Log.d("FFImporter", "detail data retrieval duration in ms: " + duration);
        return doc;
    }
}
