package ch.si.forexeconomiccalendar.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.GlobalApplication;
import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.events.entities.FFDetail;
import ch.si.forexeconomiccalendar.app.events.entities.FFHistoryEntry;
import ch.si.forexeconomiccalendar.app.events.entities.FFSpec;
import ch.si.forexeconomiccalendar.app.events.enums.Impact;
import ch.si.forexeconomiccalendar.app.exceptions.NetworkDownException;
import ch.si.forexeconomiccalendar.app.util.DayOfWeek;
import ch.si.forexeconomiccalendar.app.util.EventCategory;

public class EventDetailsActivity extends Activity {
    private ProgressDialog progress;
    private int id;
    private long time;
    private MenuItem refreshItem;


    private String title;
    private Impact impact;

    private List<FFHistoryEntry> ffHistoryEntryList;
    private List<FFSpec> ffSpecList;

    private LinearLayout linlaHeaderProgress = null;
    private LinearLayout linlaNoData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horizontal_scroll);

        // analytics tracking
        trackPage();

        Bundle b = getIntent().getExtras();
        id = b.getInt("id");
        title = b.getString("title");
        time = b.getLong("time");

        String impactString = b.getString("impact");
        if (impactString.equals(Impact.HIGH.toString())) {
            impact = Impact.HIGH;
        } else if (impactString.equals(Impact.MEDIUM.toString())) {
            impact = Impact.MEDIUM;
        } else if (impactString.equals(Impact.LOW.toString())) {
            impact = Impact.LOW;
        }

        new DetailsLoader().execute(id);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event_details, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.refresh_details) {
            reloadView();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reloadView() {
//        loadLoadingDialog();
        new DetailsLoader().execute(id);
    }

    private void loadLoadingDialog() {
        progress = new ProgressDialog(this);
        progress.setTitle("Loading...");
        progress.show();

    }

    private void loadNetworkProblemDialog() {
        AlertDialog networkProblemDialog = new AlertDialog.Builder(this)
                .setTitle("Can't access internet!")
                .setMessage("Make sure your internet is on.")
                .setPositiveButton("Got it!",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .create();
        networkProblemDialog.show();
    }

    private void closeLoadingDialog() {
        progress.dismiss();
    }

    private class DetailsLoader extends AsyncTask<Integer, Void, FFDetail> {

        private boolean showNoData = false;

        @Override
        protected void onPreExecute() {
            showLoading();
        }

        @Override
        protected FFDetail doInBackground(Integer... params) {
            int link = params[0];
            FFDetail eventDetail = null;
            try {
                eventDetail = FFDetail.importAndBuild(link);
            } catch (NetworkDownException e) {
                Log.e("EventDetailsActivity", "Network is down. Can't load eventdetails");
                showNoData = true;
            }

            return eventDetail;
        }

        @Override
        protected void onPostExecute(FFDetail eventDetail) {
            dismissLoading();
            if (showNoData) {
                showNoData();
                return;
            }

            ffSpecList = eventDetail.getSpecsList();
            ffHistoryEntryList = eventDetail.getHistoryList();

            loadView();

        }
    }

    private void loadView() {
        LinearLayout root = (LinearLayout)findViewById(R.id.activitiy_horizontal_scroll);

        Button titleButton = createButton("   " + title, impact, root);
        root.addView(titleButton);

        ScrollView verticalScrollView = (ScrollView) getLayoutInflater().inflate(R.layout.detail_sv_linearl, null);
        root.addView(verticalScrollView);

        LinearLayout sections = (LinearLayout)verticalScrollView.findViewById(R.id.detail_scrollViewVertical_ll);

        DateTime eventTimeNY = new DateTime(time).withZone(DateTimeZone.getDefault());
        String localDayOfWeek = DayOfWeek.getString(eventTimeNY.getDayOfWeek());
        int localMonth = eventTimeNY.getMonthOfYear();
        int localDay = eventTimeNY.getDayOfMonth();
        int localYear = eventTimeNY.getYear();
        int localHour = eventTimeNY.getHourOfDay();
        int localMin = eventTimeNY.getMinuteOfHour();

        String localMinString;
        if (localMin < 10) {
            localMinString = "0" + localMin;
        } else {
            localMinString = "" + localMin;
        }

        String localTime = localHour + ":" + localMinString;
        String day = localMonth + "/" + localDay + "/" + localYear;

        sections.addView(createSourceTitle("Source: forexfactory.com \n" + localDayOfWeek +", " + day + " " + localTime, sections));

        sections.addView(createParagraphTitle("History"));
        sections.addView(createHistorySection(ffHistoryEntryList));

        sections.addView(createParagraphTitle("Details"));
        sections.addView(createSpecSection(ffSpecList));

    }

    private TableLayout createSpecSection(List<FFSpec> ffSpecList) {
        TableLayout tableLayout = (TableLayout)getLayoutInflater().inflate(R.layout.detail_table, null);
        for (FFSpec ffs : ffSpecList) {
            TableRow tableRow = createEmptyTableRow(tableLayout);
            TextView tv = createTableHeader(ffs.getTitle(), tableRow);
            tableRow.addView(createTableHeader(ffs.getTitle(), tableRow));
            tableRow.addView(createTableContent(ffs.getContent(), tableRow));
            tableLayout.addView(tableRow);
        }
        return tableLayout;
    }

    private TableLayout createHistorySection(List<FFHistoryEntry> ffHistoryEntryList) {
        TableLayout tableLayout = (TableLayout)getLayoutInflater().inflate(R.layout.detail_table, null);
        List<String> headerStrings = new ArrayList<String>();
        headerStrings.add("History");
        headerStrings.add("Actual");
        headerStrings.add("Forecast");
        headerStrings.add("Previous");

        tableLayout.addView(createHeaderTableRow(headerStrings, tableLayout));

        for (FFHistoryEntry ffhe : ffHistoryEntryList) {
            List<String> tempList = new ArrayList<String>();
            tempList.add(ffhe.getTitle());
            tempList.add(ffhe.getActual());
            tempList.add(ffhe.getForecast());
            tempList.add(ffhe.getPrevious());
            tableLayout.addView(createContentTableRow(tempList, tableLayout));
        }
        return tableLayout;
    }

    // title creation

    private TextView createSourceTitle(String content, LinearLayout parent) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_source_title, parent, false);
        textView.setText(content);
        return textView;
    }

    private Button createButton(String title, Impact impact, LinearLayout parent) {
        Button button = (Button)getLayoutInflater().inflate(R.layout.detail_title, parent, false);

        Drawable img;
        switch (impact) {
            case HIGH:
                img = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_red_fixed_height);
                break;
            case MEDIUM:
                img = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_orange_fixed_height);
                break;
            case LOW:
                img = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_yellow_fixed_height);
                break;
            default:
                img = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_red_fixed_height);
                break;
        }

        button.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        button.setText(title);
        return button;
    }

    // table creation

    private TextView createTableHeader(String value, TableRow parent) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_table_header, parent, false);
        textView.setText(value);
        return textView;
    }

    private TextView createTableContent(String value, TableRow parent) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_table_content, parent, false);
        textView.setText(value);
        return textView;
    }

    private TableRow createHeaderTableRow(List<String> headerList, TableLayout parent) {
        TableRow tableRow = (TableRow)getLayoutInflater().inflate(R.layout.detail_table_row, parent, false);
        for (String header : headerList) {
            tableRow.addView(createTableHeader(header, tableRow));
        }
        return tableRow;
    }

    private TableRow createContentTableRow(List<String> contentList, TableLayout parent) {
        TableRow tableRow = (TableRow)getLayoutInflater().inflate(R.layout.detail_table_row, parent, false);
        for (String content : contentList) {
            tableRow.addView(createTableContent(content, tableRow));
        }
        return tableRow;
    }

    private TableRow createEmptyTableRow(TableLayout parent) {
        return (TableRow)getLayoutInflater().inflate(R.layout.detail_table_row, parent, false);
    }

    private HorizontalScrollView createTableSection(List<List<String>> tableContent) {
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView)getLayoutInflater().inflate(R.layout.detail_table, null);
        TableLayout table = (TableLayout)horizontalScrollView.findViewById(R.id.detail_table);
        for (int i = 0; i < tableContent.size(); i++) {
            if (i == 0) {
                table.addView(createHeaderTableRow(tableContent.get(0), table));
            } else {
                table.addView(createContentTableRow(tableContent.get(i), table));
            }
        }
        return horizontalScrollView;
    }


    // content creation

    private TextView createParagraphTitle(String title) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_paragraph_title, null);
        textView.setText(title);
        return textView;
    }

    private TextView createParagraphContent(String value) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_paragraph_content, null);
        textView.setText(value);
        return textView;
    }

//    private LinearLayout createContentSection(List<EventDetailParagraph> edpList) {
//        LinearLayout linearLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.detail_paragraphs_ll, null);
//        for (EventDetailParagraph edp : edpList) {
//            linearLayout.addView(createParagraphTitle(edp.getTitle()));
//            linearLayout.addView(createParagraphContent(edp.getContent()));
//        }
//        return linearLayout;
//    }

    private void trackPage() {
        Tracker t = ((GlobalApplication)getApplication()).getTracker(GlobalApplication.TrackerName.APP_TRACKER);
        t.setScreenName("ch.si.forexeconomiccalendar.EventDetailsActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (linlaHeaderProgress == null) {
                    linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
                }
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        });
    }

    private void dismissLoading() {
        Log.d("EventsSectionFragment" , "dismiss: loading");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("EventsSectionFragment", "linlaHeaderProgress: " + linlaHeaderProgress);
                if (linlaHeaderProgress == null) {
                    linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
                }
                if (linlaHeaderProgress != null) {
                    linlaHeaderProgress.setVisibility(View.GONE);
                }
            }
        });

    }

    private void showNoData() {
        Log.d("EventsSectionFragment" , "show: no data");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (linlaNoData == null) {
                    linlaNoData = (LinearLayout) findViewById(R.id.linlaNoData);
                }
                linlaNoData.setVisibility(View.VISIBLE);

                Button retryButton = (Button) linlaNoData.findViewById(R.id.retry_button);
                retryButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        retry();
                    }
                });
            }
        });
    }

    private void dismissNoData() {
        Log.d("EventsSectionFragment", "dismissing: no data");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (linlaNoData == null) {
                    linlaNoData = (LinearLayout) findViewById(R.id.linlaNoData);
                }
                linlaNoData.setVisibility(View.GONE);
            }
        });
    }

    private void retry() {
        dismissNoData();
        new DetailsLoader().execute(id);
    }

}
