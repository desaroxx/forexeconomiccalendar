package ch.si.forexeconomiccalendar.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.R;

public class HorizontalScroll extends Activity {

//    private boolean hasTable = false;
//    private List<List<String>> tableContent;
//    private List<EventDetailParagraph> paragraphList;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_horizontal_scroll);
//
//        // table sample data
//        hasTable = false;
//
//        tableContent = new ArrayList<List<String>>();
//
//        for (int j = 0; j < 5; j++) {
//            List<String> tableRow = new ArrayList<String>();
//            for (int i = 0; i < 10; i++) {
//                tableRow.add("Content " + (i+1));
//            }
//            tableContent.add(tableRow);
//        }
//
//        // content sample data
//        paragraphList = new ArrayList<EventDetailParagraph>();
//
//        paragraphList.add(new EventDetailParagraph("Header 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis purus rutrum, pretium arcu nec, malesuada leo. Etiam ac leo eu lacus vehicula auctor vel eget leo. Pellentesque in ultricies nisi. Morbi adipiscing quis leo sed laoreet. Pellentesque quis aliquet dolor, suscipit aliquet magna. Proin urna velit, elementum dictum mattis quis, interdum ac arcu. Aenean arcu metus, vestibulum ut enim ac, lobortis mattis nisi. Ut laoreet mattis consectetur. In molestie, arcu posuere molestie feugiat, velit urna hendrerit sapien, ac volutpat tortor odio non nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec porta vulputate vestibulum. Vestibulum gravida urna dolor, et tempor neque dignissim non. Pellentesque eget arcu et libero malesuada volutpat feugiat ut diam. Nulla nec arcu in massa pulvinar gravida. Integer erat diam, rhoncus et aliquam eget, hendrerit eu leo. Fusce vel risus quis ante feugiat rutrum et in urna."));
//        paragraphList.add(new EventDetailParagraph("Header 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis purus rutrum, pretium arcu nec, malesuada leo. Etiam ac leo eu lacus vehicula auctor vel eget leo. Pellentesque in ultricies nisi. Morbi adipiscing quis leo sed laoreet. Pellentesque quis aliquet dolor, suscipit aliquet magna. Proin urna velit, elementum dictum mattis quis, interdum ac arcu. Aenean arcu metus, vestibulum ut enim ac, lobortis mattis nisi. Ut laoreet mattis consectetur. In molestie, arcu posuere molestie feugiat, velit urna hendrerit sapien, ac volutpat tortor odio non nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec porta vulputate vestibulum. Vestibulum gravida urna dolor, et tempor neque dignissim non. Pellentesque eget arcu et libero malesuada volutpat feugiat ut diam. Nulla nec arcu in massa pulvinar gravida. Integer erat diam, rhoncus et aliquam eget, hendrerit eu leo. Fusce vel risus quis ante feugiat rutrum et in urna."));
//        paragraphList.add(new EventDetailParagraph("Header 3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis purus rutrum, pretium arcu nec, malesuada leo. Etiam ac leo eu lacus vehicula auctor vel eget leo. Pellentesque in ultricies nisi. Morbi adipiscing quis leo sed laoreet. Pellentesque quis aliquet dolor, suscipit aliquet magna. Proin urna velit, elementum dictum mattis quis, interdum ac arcu. Aenean arcu metus, vestibulum ut enim ac, lobortis mattis nisi. Ut laoreet mattis consectetur. In molestie, arcu posuere molestie feugiat, velit urna hendrerit sapien, ac volutpat tortor odio non nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec porta vulputate vestibulum. Vestibulum gravida urna dolor, et tempor neque dignissim non. Pellentesque eget arcu et libero malesuada volutpat feugiat ut diam. Nulla nec arcu in massa pulvinar gravida. Integer erat diam, rhoncus et aliquam eget, hendrerit eu leo. Fusce vel risus quis ante feugiat rutrum et in urna."));
//
//        loadView();
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.horizontal_scroll, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private void loadView() {
//        LinearLayout root = (LinearLayout)findViewById(R.id.activitiy_horizontal_scroll);
//        root.removeAllViews();
//
//        Button titleButton = createButton("Sample Title");
//        root.addView(titleButton);
//
//        ScrollView verticalScrollView = (ScrollView) getLayoutInflater().inflate(R.layout.detail_sv_linearl, null);
//        root.addView(verticalScrollView);
//
//        LinearLayout sections = (LinearLayout)verticalScrollView.findViewById(R.id.detail_scrollViewVertical_ll);
//
//        if (hasTable) {
//            sections.addView(createTableSection(tableContent));
//        }
//        sections.addView(createContentSection(paragraphList));
//
//    }
//
//    // title creation
//
//    private Button createButton(String title) {
//        Button button = (Button)getLayoutInflater().inflate(R.layout.detail_title, null);
//        button.setText(title);
//        return button;
//    }
//
//    // table creation
//
//    private TextView createTableHeader(String value, TableRow parent) {
//        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_table_header, parent, false);
//        textView.setText(value);
//        return textView;
//    }
//
//    private TextView createTableContent(String value, TableRow parent) {
//        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_table_content, parent, false);
//        textView.setText(value);
//        return textView;
//    }
//
//    private TableRow createHeaderTableRow(List<String> headerList, TableLayout parent) {
//        TableRow tableRow = (TableRow)getLayoutInflater().inflate(R.layout.detail_table_row, parent, false);
//        for (String header : headerList) {
//            tableRow.addView(createTableHeader(header, tableRow));
//        }
//        return tableRow;
//    }
//
//    private TableRow createContentTableRow(List<String> contentList, TableLayout parent) {
//        TableRow tableRow = (TableRow)getLayoutInflater().inflate(R.layout.detail_table_row, parent, false);
//        for (String content : contentList) {
//            tableRow.addView(createTableContent(content, tableRow));
//        }
//        return tableRow;
//    }
//
//    private HorizontalScrollView createTableSection(List<List<String>> tableContent) {
//        HorizontalScrollView horizontalScrollView = (HorizontalScrollView)getLayoutInflater().inflate(R.layout.detail_horizontalsv_table, null);
//        TableLayout table = (TableLayout)horizontalScrollView.findViewById(R.id.detail_table);
//        for (int i = 0; i < tableContent.size(); i++) {
//            if (i == 0) {
//                table.addView(createHeaderTableRow(tableContent.get(0), table));
//            } else {
//                table.addView(createContentTableRow(tableContent.get(i), table));
//            }
//        }
//        return horizontalScrollView;
//    }
//
//
//    // content creation
//
//    private TextView createParagraphTitle(String title) {
//        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_paragraph_title, null);
//        textView.setText(title);
//        return textView;
//    }
//
//    private TextView createParagraphContent(String value) {
//        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.detail_paragraph_content, null);
//        textView.setText(value);
//        return textView;
//    }
//
//    private LinearLayout createContentSection(List<EventDetailParagraph> edpList) {
//        LinearLayout linearLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.detail_paragraphs_ll, null);
//        for (EventDetailParagraph edp : edpList) {
//            linearLayout.addView(createParagraphTitle(edp.getTitle()));
//            linearLayout.addView(createParagraphContent(edp.getContent()));
//        }
//        return linearLayout;
//    }

}
