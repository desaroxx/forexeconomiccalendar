package ch.si.forexeconomiccalendar.app.persistance;

import android.content.Context;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.alarms.NotificationEntity;
import ch.si.forexeconomiccalendar.app.persistance.adapters.InternalStoragePersister;

/**
 * Created by kensteiner on 30/06/14.
 */
public class NotificationsPersister {
    final String NOTIFICATIONS_PERSISTER_FILENAME = "NOTIFICATIONS";

    private Context context;

    public NotificationsPersister(Context context) {
        this.context = context;
    }

    private void store(List<NotificationEntity> notificationEntityList) {
        Log.d("NotificationsPersister", "storing " + notificationEntityList.size() + " items");
        InternalStoragePersister.write(NOTIFICATIONS_PERSISTER_FILENAME, notificationEntityList, context);
    }

    public List<NotificationEntity> retrieve(){
        List<NotificationEntity> retrievedList;
        try {
            retrievedList = (List<NotificationEntity>)InternalStoragePersister.read(NOTIFICATIONS_PERSISTER_FILENAME, context);
            if (retrievedList == null) {
                retrievedList = new ArrayList<NotificationEntity>();
            }
        } catch (Exception e) {
            retrievedList = new ArrayList<NotificationEntity>();
        }
        return retrievedList;
    }

    public void add(NotificationEntity notificationEntity) {
        List<NotificationEntity> notificationEntityList = retrieve();
        notificationEntityList.add(notificationEntity);
        store(notificationEntityList);
    }

    public void add(List<NotificationEntity> notificationEntityList) {
        List<NotificationEntity> notificationEntityToPersist = retrieve();
        notificationEntityToPersist.addAll(notificationEntityList);
        store(notificationEntityToPersist);
    }

    public void removeOne(int uuidHash) {
        List<NotificationEntity> notificationEntityList = retrieve();
        for(NotificationEntity ne : notificationEntityList) {
            if(ne.getUuid().hashCode()==uuidHash) {
                notificationEntityList.remove(ne);
                store(notificationEntityList);
                return;
            }
        }
        Log.e("NotificationsPersister","Failed to remove entity");
    }

    public List<NotificationEntity> removeAll() {
        List<NotificationEntity> notificationEntityList = retrieve();
        store(new ArrayList<NotificationEntity>());
        return notificationEntityList;
    }

    public NotificationEntity find(int uuidHash) {
        List<NotificationEntity> notificationEntityList = retrieve();
        for(NotificationEntity ne : notificationEntityList) {
            if(ne.getUuid().hashCode()==uuidHash) {
                removeOld();
                return ne;
            }
        }
        Log.e("NotificationsPersister", "Failed to find Entity with matching uuidHash");
        return null;
    }

    public List<NotificationEntity> removeOld(List<NotificationEntity> notificationEntityList) {
        List<NotificationEntity> returnList = new ArrayList<NotificationEntity>();
        long time24hAgo = DateTime.now(DateTimeZone.UTC).minusDays(1).getMillis();
        int counter = notificationEntityList.size();
        for(NotificationEntity ne : notificationEntityList) {
            long eventTime = ne.getEventTime().withZone(DateTimeZone.UTC).getMillis();
            if(eventTime > time24hAgo) {
                returnList.add(ne);
                counter--;
            }
        }
        Log.d("NotificationsPersister","removed " + counter + " old entries");
        return returnList;
    }

    public void removeOld() {
        store(removeOld(retrieve()));
    }

}
