package ch.si.forexeconomiccalendar.app.exceptions;

/**
 * Created by kensteiner on 04/07/14.
 */
public class NetworkDownException extends Exception {
    public NetworkDownException() { super(); }

    public NetworkDownException(String message) {
        super(message);
    }
}
