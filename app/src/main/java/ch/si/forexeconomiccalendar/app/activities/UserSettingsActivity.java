package ch.si.forexeconomiccalendar.app.activities;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spanned;
import android.text.SpannedString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


import org.joda.time.DateTime;

import java.util.Map;

import ch.si.forexeconomiccalendar.app.GlobalApplication;
import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.alarms.NotificationsManager;
import ch.si.forexeconomiccalendar.app.dialogs.CurrencyChooserDialogFragment;
import ch.si.forexeconomiccalendar.app.dialogs.ImpactChooserDialogFragment;
import ch.si.forexeconomiccalendar.app.dialogs.MinutesChooserDialogFragment;
import ch.si.forexeconomiccalendar.app.dialogs.MuteSoundChooserDialogFragment;
import ch.si.forexeconomiccalendar.app.dialogs.UnmuteConfirmDialogFragment;
import ch.si.forexeconomiccalendar.app.events.enums.Currency;
import ch.si.forexeconomiccalendar.app.events.enums.Impact;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;
import ch.si.forexeconomiccalendar.app.util.coloredstring.SpannableBuilder;

/**
 * Created by kensteiner on 27/07/14.
 */
public class UserSettingsActivity extends Activity
        implements
            MinutesChooserDialogFragment.MinutesChooserDialogListener,
            ImpactChooserDialogFragment.ImpactChooserDialogListener,
            CurrencyChooserDialogFragment.CurrencyChooserDialogListener,
            MuteSoundChooserDialogFragment.MuteSoundChooserDialogListener,
            UnmuteConfirmDialogFragment.UnmuteConfirmDialogListener{

    private UserSettingsPersister userSettingsPersister;
    private Map<Impact,Boolean> impactMap;
    private Map<Currency, Boolean> currencyMap;
    private boolean alertActivated;
    private boolean alertSoundActivated;
    private long alertSoundDeactivatedUntil;
    private int alertTime;
    private Tracker tracker;

    private Button alertTimeBtn;
    private TextView activeImpactsTextView;
    private TextView activeCurrenciesTextView;
    private Button alertSoundBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("UserSettingsActivity", "onCreate called");
        setContentView(R.layout.activity_source_settings);

        initialize();

        loadView();

        Intent intent = getIntent();
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        if (extras != null) {
            if (extras.containsKey("interest")) {
                Log.d("UserSettingsActivity", "extras contains interest");
                String interest = extras.getString("interest");
                Log.d("UserSettingsActivity", "interest: " + interest);

                NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                if (interest.equals("mute")) {
                    showMuteSoundChooserDialog();
                    // remove notification from notification center
                    mNotificationManager.cancel(1);
                } else if (interest.equals("unmute")) {
                    showUnmuteConfirmDialog();
                    // remove notification from notification center
                    mNotificationManager.cancel(1);
                } else if(interest.equals("edit_settings")) {
                    // remove notification from notification center
                    mNotificationManager.cancel(1);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("UserSettingsActivity", "onResume called");
    }

    private void initialize() {
        userSettingsPersister = new UserSettingsPersister(getApplicationContext());
        impactMap = userSettingsPersister.readImpactSettings();
        currencyMap = userSettingsPersister.readCurrencySettings();

        alertActivated = userSettingsPersister.readNotificationActivated();
        alertSoundDeactivatedUntil = userSettingsPersister.readNotificationSoundDeactivatedUntil();
        long timeNow = DateTime.now().getMillis();
        boolean stillDeactivated = (alertSoundDeactivatedUntil - timeNow) + 2L > 0L;

        if (stillDeactivated) {
            alertSoundActivated = false;
        } else {
            alertSoundActivated = userSettingsPersister.readNotificationSoundActivated();
        }

        alertTime = userSettingsPersister.readNotificationTime();

        // analytics tracking
        tracker = ((GlobalApplication)getApplication()).getTracker(GlobalApplication.TrackerName.APP_TRACKER);
        trackPage(tracker);
    }

    private void loadView() {
        LinearLayout root = (LinearLayout)findViewById(R.id.settings_main_container);

        loadFilterSection(root);
        loadAlertSection(root);
    }

    private void loadFilterSection(LinearLayout parent) {
        LinearLayout section = (LinearLayout)getLayoutInflater().inflate(R.layout.settings_section, parent, false);

        section.addView(createSectionTitle("Filters", section));
        loadImpactSubsection(section);
        loadCurrencySubsection(section);

        parent.addView(section);
    }

    private void loadImpactSubsection(LinearLayout parent) {
        parent.addView(createSectionDescription(new SpannedString("Selected impact categories:"), parent));
        Spanned activeImpactsSpanned = getActiveImpactsCharSequence();
        activeImpactsTextView = createSectionDescription(activeImpactsSpanned, parent);
        parent.addView(activeImpactsTextView);

        Button editImpactsButton = (Button)getLayoutInflater().inflate(R.layout.settings_button_edit, parent, false);
        editImpactsButton.setText("Edit categories");
        editImpactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImpactSelectionDialog();
            }
        });
        parent.addView(editImpactsButton);
    }

    private void loadCurrencySubsection(LinearLayout parent) {
        parent.addView(createSectionDescription(new SpannedString("Selected currencies:"), parent));
        Spanned activeCurrenciesSpanned = getActiveCurrencies();
        activeCurrenciesTextView = createSectionDescription(activeCurrenciesSpanned, parent);
        parent.addView(activeCurrenciesTextView);

        Button editCurrenciesButton = (Button)getLayoutInflater().inflate(R.layout.settings_button_edit, parent, false);
        editCurrenciesButton.setText("Edit currencies");
        editCurrenciesButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCurrencySelectionDialog();
            }
        });

        parent.addView(editCurrenciesButton);
    }

    private void loadAlertSection(LinearLayout parent) {
        LinearLayout section = (LinearLayout)getLayoutInflater().inflate(R.layout.settings_section, parent, false);

        section.addView(createSectionTitle("Event Reminders", parent));
        section.addView(createSectionDescription(new SpannedString("Do you want reminders?"), parent));
        section.addView(createAlertActivatedButton(parent));
        alertTimeBtn = createAlertTimeButton(parent);
        section.addView(alertTimeBtn);
        alertSoundBtn = createAlertSoundButton(parent);
        section.addView(alertSoundBtn);

        parent.addView(section);

    }

    private Button createAlertTimeButton(LinearLayout parent) {
        Button button = (Button)getLayoutInflater().inflate(R.layout.settings_button_edit, parent, false);
        button.setText(getTimeString(alertTime));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });
        return button;
    }

    private Button createAlertSoundButton(LinearLayout parent) {
        final Button button = (Button)getLayoutInflater().inflate(R.layout.settings_button_mute, parent, false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("UserSettingsActivity", "alertSoundButton onClick: toggled from " + alertSoundActivated + " to" + !alertSoundActivated);
                if (alertSoundActivated) {
                    // if was set to ON
                    showMuteSoundChooserDialog();
                } else {
                    // if was set to OFF
                    Drawable soundActiveIcon = getApplicationContext().getResources().getDrawable(R.drawable.ic_unmute);
                    button.setCompoundDrawablesWithIntrinsicBounds(null, null, soundActiveIcon, null);
                    button.setText("Alert sound: ON");
                    alertSoundActivated = true;

                    userSettingsPersister.updateNotificationSoundActivated(true);
                    userSettingsPersister.updateNotificationSoundDeactivatedUntil(-1L);
                }

            }
        });

        Drawable drawable;
        if (alertSoundActivated) {
            drawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_unmute);
            button.setText("Alert sound: ON");
        } else {
            drawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_mute);
            button.setText("Alert sound: OFF");
        }
        button.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);

        return button;
    };

    private Button createAlertActivatedButton(LinearLayout parent) {
        final Button button = (Button)getLayoutInflater().inflate(R.layout.settings_button_receive_alerts, parent, false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable check;
                if (alertActivated) {
                    alertActivated = false;
                    check = getApplicationContext().getResources().getDrawable(R.drawable.ic_uncheck);
                } else {
                    alertActivated = true;
                    check = getApplicationContext().getResources().getDrawable(R.drawable.ic_check);
                }
                userSettingsPersister.writeNotificationActivated(alertActivated);
                button.setCompoundDrawablesWithIntrinsicBounds(null, null, check, null);
            }
        });

        Drawable check;
        if (alertActivated) {
            check = getApplicationContext().getResources().getDrawable(R.drawable.ic_check);
        } else {
            check = getApplicationContext().getResources().getDrawable(R.drawable.ic_uncheck);
        }
        button.setCompoundDrawablesWithIntrinsicBounds(null, null, check, null);

        return button;
    }

    private TextView createSectionTitle(String title, LinearLayout parent) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.settings_heading, parent, false);
        textView.setText(title);
        return textView;
    }

    private TextView createSectionDescription(Spanned content, LinearLayout parent) {
        TextView textView = (TextView)getLayoutInflater().inflate(R.layout.settings_explanation, parent, false);
        textView.setText(content);

//        Spannable spannable = new SpannableString("▌");
//        spannable.setSpan(new ForegroundColorSpan(0xffe0162b), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        textView.setText(spannable);
//        Spannable wordtoSpan = new SpannableString("I know just how to whisper, And I know just how to cry,I know just where to find the answers");
//        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), 15, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        Spannable wordtoSpan2 = new SpannableString("I know just how to whisper, And I know just how to cry,I know just where to find the answers");
//        wordtoSpan2.setSpan(new ForegroundColorSpan(Color.BLUE), 15, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        Spanned spanned = (Spanned) TextUtils.concat(wordtoSpan, wordtoSpan2);
//        textView.setText(spanned);
        return textView;
    }

    private void show() {
        DialogFragment newFragment = new MinutesChooserDialogFragment();
        newFragment.show(getFragmentManager(), "alert_time_fragment");
    }

    private void showImpactSelectionDialog() {
        DialogFragment fragment = new ImpactChooserDialogFragment();
        fragment.show(getFragmentManager(), "impact_fragment");
    }

    private void showCurrencySelectionDialog() {
        DialogFragment fragment = new CurrencyChooserDialogFragment();
        fragment.show(getFragmentManager(), "currency_fragment");
    }

    private void showMuteSoundChooserDialog() {
        DialogFragment fragment = new MuteSoundChooserDialogFragment();
        fragment.show(getFragmentManager(), "mutesound_fragment");
    }

    private void showUnmuteConfirmDialog() {
        DialogFragment fragment = new UnmuteConfirmDialogFragment();
        fragment.show(getFragmentManager(), "unmuteconfirm_fragment");
    }

//    private Button createImpactButton(final Impact impact, LinearLayout parent) {
//        final Button button = (Button)getLayoutInflater().inflate(R.layout.settings_button_impact, parent, false);
//        button.setText("   " + impact.toString());
//
//        final Drawable leftImg;
//        switch (impact) {
//            case HIGH: leftImg = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_red_fixed_height); break;
//            case MEDIUM: leftImg = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_orange_fixed_height); break;
//            case LOW: leftImg = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_yellow_fixed_height); break;
//            default: leftImg = getApplicationContext().getResources().getDrawable(R.drawable.rectangle_neutral_fixed_height); break;
//        }
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (activeImpactsTextView.get(impact)) {
//                    activeImpactsTextView.put(impact, false);
//                    Drawable uncheckImg = getApplicationContext().getResources().getDrawable(R.drawable.ic_uncheck);
//                    button.setCompoundDrawablesWithIntrinsicBounds(leftImg, null, uncheckImg, null);
//                    userSettingsPersister.updateImpactSettings(impact, false);
//                } else {
//                    activeImpactsTextView.put(impact, true);
//                    Drawable checkImg = getApplicationContext().getResources().getDrawable(R.drawable.ic_check);
//                    button.setCompoundDrawablesWithIntrinsicBounds(leftImg, null, checkImg, null);
//                    userSettingsPersister.updateImpactSettings(impact, true);
//                }
//            }
//        });
//
//        Drawable checked;
//        if(activeImpactsTextView.get(impact)) {
//            checked = getApplicationContext().getResources().getDrawable(R.drawable.ic_check);
//
//        } else {
//            checked = getApplicationContext().getResources().getDrawable(R.drawable.ic_uncheck);
//        }
//        button.setCompoundDrawablesWithIntrinsicBounds(leftImg, null, checked, null);
//
//        return button;
//    }

    @Override
    public void update() {
        Log.d("SourceSettingsActivity", "MinutesChooserFragment update called");
        setButtonTitleEventReminder();
    }

    @Override
    public void onUpdateImpactChooser() {
        Log.d("SourceSettingsActivity", "ImpactChooserFragment update called");
        impactMap = userSettingsPersister.readImpactSettings();
        activeImpactsTextView.setText(getActiveImpactsCharSequence());
    }

    @Override
    public void onUpdateCurrencyChooser() {
        Log.d("SourceSettingsActivity", "CurrencyChooserFragment update called");
        currencyMap = userSettingsPersister.readCurrencySettings();
        activeCurrenciesTextView.setText(getActiveCurrencies());
    }

    @Override
    public void onUpdateMuteSoundChooser() {
        alertSoundActivated = userSettingsPersister.readNotificationSoundActivated();
        alertSoundDeactivatedUntil = userSettingsPersister.readNotificationSoundDeactivatedUntil();

        long timeNow = DateTime.now().getMillis();
        boolean stillDeactivated = (alertSoundDeactivatedUntil - timeNow) + 2L > 0L;

        if (stillDeactivated) {
            alertSoundActivated = false;
        } else {
            alertSoundActivated = userSettingsPersister.readNotificationSoundActivated();
        }

        Drawable drawable;
        if (alertSoundActivated) {
            drawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_unmute);
            alertSoundBtn.setText("Alert sound: ON");
        } else {
            drawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_mute);
            alertSoundBtn.setText("Alert sound: OFF");
        }
        alertSoundBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
    }

    @Override
    public void onUpdateUnmuteConfirm() {
        Drawable soundActiveIcon = getApplicationContext().getResources().getDrawable(R.drawable.ic_unmute);
        alertSoundBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, soundActiveIcon, null);
        alertSoundBtn.setText("Alert sound: ON");
        alertSoundActivated = true;
    }

    private void setButtonTitleEventReminder() {
        int offset = new UserSettingsPersister(getApplicationContext()).readNotificationTime();
        String currentOffsetString = getTimeString(offset);
        alertTimeBtn.setText(currentOffsetString);
    }

    private void trackPage(Tracker t) {
        t.setScreenName("ch.si.forexeconomiccalendar.SourceSettingsActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.source_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    private String getTimeString(int time) {
        if (time == 0) {
            return "Alert on event";
        }
        String outputString = "";
        int tempTime = time;
        boolean before = false;
        if (tempTime < 0) {
            tempTime *= -1;
            before = true;
        }

        if(tempTime >= 1400) {
            int days = tempTime / 1400;
            outputString += days + " days ";
            tempTime = tempTime % 1400;
        }
        if (tempTime >= 60) {
            int hours = tempTime / 60;
            outputString += hours + " hours ";
            tempTime = tempTime % 60;
        }
        if (tempTime > 0) {
            int minutes = tempTime;
            outputString += minutes + " minutes ";
        }

        if (before) {
            outputString += "before ";
        } else {
            outputString += "after ";
        }

        outputString += "event";

        return outputString;
    }

    @Override
    protected void onStop() {
        super.onStop();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                new NotificationsManager(getApplicationContext()).updateNotifications();
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                Log.d("SourceSettingsActivity", "Async task finished");
            }
        }.execute();
    }

    private Spanned getActiveImpactsCharSequence() {
        return SpannableBuilder.createColoredImpacts(impactMap);
    }

    private Spanned getActiveCurrencies() {
        String activeCurrenciesString = "";

        boolean atLeastOneEntry = false;
        for (Map.Entry<Currency,Boolean> entry : currencyMap.entrySet()) {
            if (entry.getValue()) {
                if (atLeastOneEntry) {
                    activeCurrenciesString += ",";
                }
                activeCurrenciesString += " " + entry.getKey().toString();
                atLeastOneEntry = true;
            }
        }

        if (!atLeastOneEntry) {
            activeCurrenciesString = "(none selected)";
        }
        return new SpannedString(activeCurrenciesString);
    }
}
