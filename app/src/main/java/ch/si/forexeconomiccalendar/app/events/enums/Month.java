package ch.si.forexeconomiccalendar.app.events.enums;

/**
 * Created by kensteiner on 18/07/14.
 */
public enum Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    Decemeber
}
