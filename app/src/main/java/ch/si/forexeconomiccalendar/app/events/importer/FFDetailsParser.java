package ch.si.forexeconomiccalendar.app.events.importer;


import android.util.Log;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.events.entities.FFHistoryEntry;
import ch.si.forexeconomiccalendar.app.events.entities.FFSpec;


/**
 * Created by kensteiner on 19/07/14.
 */
public class FFDetailsParser {
    private Document inputDocument;

    public FFDetailsParser(Document inputDocument) {
        this.inputDocument = inputDocument;
    }

    public List<FFSpec> getSpecs() {
        List<FFSpec> specList = new ArrayList<FFSpec>();
        Log.d("FFDetailsParser", "imported: \n" + inputDocument);
        Elements elements = inputDocument.select("table.calendarspecs");
        Elements specElements = elements.get(1).select("tr");
        String title;
        String content;
        for(Element e : specElements) {
            title = e.child(0).text();
            content = e.child(1).text();
            specList.add(new FFSpec(title, content));
            Log.d("FFDetailsParser", "spec -> t:" + title + " ,c: " + content);
        }
        return specList;
    }

    public List<FFHistoryEntry> getHistory() {
        List<FFHistoryEntry> historyList = new ArrayList<FFHistoryEntry>();
        Elements elements = inputDocument.select("table.alternating");
        Elements historyElements = elements.first().select("tr[class]");

        String date;
        String actual;
        String forecast;
        String previous;
        Element historyElement;

        for (int i = 0; i < historyElements.size(); i++) {
            historyElement = historyElements.get(i);

            date = historyElement.child(0).child(0).text();
            actual = historyElement.select(".actual").first().text();
            forecast = historyElement.select(".forecast").first().text();
            previous = historyElement.select(".previous").text();
            // TODO: add revised

            historyList.add(new FFHistoryEntry(date, actual, forecast, previous));
            Log.d("FFDetailsParser", "history -> d:" + date + " ,a: " + actual + " ,f: " + forecast + " ,p: " + previous);
        }

        return historyList;
    }

}
