package ch.si.forexeconomiccalendar.app.events.entities;


import java.io.Serializable;

import ch.si.forexeconomiccalendar.app.events.enums.Performance;

/**
 * Created by kensteiner on 19/07/14.
 */
public class PerformanceEntity implements Serializable {
    private String value;
    private Performance performance;

    public PerformanceEntity(String value, Performance performance) {
        this.value = value;
        this.performance = performance;
    }

    public String getValue() {
        return value;
    }

    public Performance getPerformance() {
        return performance;
    }
}
