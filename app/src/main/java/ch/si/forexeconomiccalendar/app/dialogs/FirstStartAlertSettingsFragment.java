package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;

import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 11/07/14.
 */
public class FirstStartAlertSettingsFragment extends DialogFragment  {

    public static FirstStartAlertSettingsFragment newInstance() {
        FirstStartAlertSettingsFragment frag = new FirstStartAlertSettingsFragment();
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle("Event Reminders")
                .setMessage("Receive reminders for important, upcoming events?")
                .setPositiveButton(Html.fromHtml("<b>Yes</b>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                new UserSettingsPersister(getActivity().getApplicationContext()).writeNotificationActivated(true);
                                MinutesChooserDialogFragment mcdf = MinutesChooserDialogFragment.newInstance();
                                mcdf.show(getActivity().getFragmentManager(), "");
                            }
                        }
                )
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        })
                .create();
    }
}