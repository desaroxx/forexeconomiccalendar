package ch.si.forexeconomiccalendar.app.alarms;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;
import ch.si.forexeconomiccalendar.app.persistance.NotificationsPersister;

/**
 * When the alarm fires, this WakefulBroadcastReceiver receives the broadcast Intent
 * and then starts the IntentService {@code SchedulingService} to do some work.
 */
public class LocalNotificationsAlarmReceiver extends WakefulBroadcastReceiver {

    private final int SYNC_TYPE = 0;
    private final int NOTIFICATION_TYPE = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        // BEGIN_INCLUDE(alarm_onreceive)
        /*
         * If your receiver intent includes extras that need to be passed along to the
         * service, use setComponent() to indicate that the service should handle the
         * receiver's intent. For example:
         *
         * ComponentName comp = new ComponentName(context.getPackageName(),
         *      MyService.class.getName());
         *
         * // This intent passed in this call will include the wake lock extra as well as
         * // the receiver intent contents.
         * startWakefulService(context, (intent.setComponent(comp)));
         *
         * In this example, we simply create a new intent to deliver to the service.
         * This intent holds an extra identifying the wake lock.
         */

        Intent service = new Intent(context, SchedulingService.class);

        // decide if: SYNC_TYPE or NOTIFICATION_TYPE
        Bundle bundle = intent.getExtras();
        int alarmType = bundle.getInt("alarmType");
        if(alarmType == SYNC_TYPE) {
            // handle SYNC_TYPE
            Log.d("LocalNotificationsAlarmReceiver", "onReceive: SYNC_TYPE");
            service.putExtra("alarmType", SYNC_TYPE);
        } else {
            // handle NOTIFICATION_TYPE
            Log.d("LocalNotificationsAlarmReceiver", "onReceive: NOTIFICATION_TYPE");
            if(bundle.containsKey("uuid-hash")) {
                service.putExtra("uuid-hash", bundle.getInt("uuid-hash"));
            } else {
                Log.e("LocalNotificationsAlarmReceiver", "bundle didn't receive uuid");
            }
            service.putExtra("alarmType", NOTIFICATION_TYPE);
        }

        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, service);
        // END_INCLUDE(alarm_onreceive)
    }

    // BEGIN_INCLUDE(set_alarm)
    /**
     * Sets a repeating alarm that runs once a day at approximately 8:30 a.m. When the
     * alarm fires, the app broadcasts an Intent to this WakefulBroadcastReceiver.
     * @param context
     */
    public void setAlarm(Context context, NotificationEntity notificationEntity) {
        Log.d("LocalNotificationsAlarmReceiver", "setAlarm");

        activateBootReceiver(context);

        int offsetInMinutes = new UserSettingsPersister(context).readNotificationTime();

        // abort if sync is inactive
        if(!isSyncActive(context)) {
            return;
        }

        // discard if event is in the past
        long timeNow = DateTime.now(DateTimeZone.UTC).getMillis();
        long eventTime = notificationEntity.getEventTime().withZone(DateTimeZone.UTC).getMillis();
        if(eventTime < timeNow) {
            Log.d("LocalNotificationsAlarmReceiver", "setAlarm: discarding past event: " + notificationEntity.getTitle());
            return;
        }

        // persist
        NotificationsPersister np = new NotificationsPersister(context);
        np.add(notificationEntity);

        // create intent with uuid
        Intent intent = new Intent(context, LocalNotificationsAlarmReceiver.class);
        intent.putExtra("uuid-hash", notificationEntity.getUuid().hashCode());
        intent.putExtra("alarmType", NOTIFICATION_TYPE);

        // get alarm manager
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        // create unique PendingIntent
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, notificationEntity.getUuid().hashCode(), intent, 0);

        // calculate notification time
        DateTime alarmTime = notificationEntity.getEventTime().toDateTime(DateTimeZone.UTC).plusMinutes(offsetInMinutes);

        Log.d("LocalNotificationsAlarmReceiver", "setAlarm to: " + alarmTime);
        // setRepeating
        if(Build.VERSION.SDK_INT >= 19 ) {
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, alarmTime.getMillis(), alarmIntent);
        } else {
            alarmMgr.set(AlarmManager.RTC_WAKEUP, alarmTime.getMillis(), alarmIntent);
        }
    }


    public void setAlarms(Context context, List<NotificationEntity> notificationEntityList) {
        Log.d("LocalNotificationsAlarmReceiver", "setAlarm");

        activateBootReceiver(context);

        int offsetInMinutes = new UserSettingsPersister(context).readNotificationTime();

        // abort if sync is inactive
        if(!isSyncActive(context)) {
            return;
        }

        List<NotificationEntity> notificationsToPersist = new ArrayList<NotificationEntity>();
        // get alarm manager
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent alarmIntent;

        for (NotificationEntity notificationEntity : notificationEntityList) {
            // discard if event is in the past
            long timeNow = DateTime.now(DateTimeZone.UTC).getMillis();
            long eventTime = notificationEntity.getEventTime().withZone(DateTimeZone.UTC).getMillis();
            if(eventTime < timeNow) {
                Log.d("LocalNotificationsAlarmReceiver", "setAlarm: discarding past event: " + notificationEntity.getTitle());
                continue;
            }

            // persist
            notificationsToPersist.add(notificationEntity);

            // create intent with uuid
            Intent intent = new Intent(context, LocalNotificationsAlarmReceiver.class);
            intent.putExtra("uuid-hash", notificationEntity.getUuid().hashCode());
            intent.putExtra("alarmType", NOTIFICATION_TYPE);

            // create unique PendingIntent
            alarmIntent = PendingIntent.getBroadcast(context, notificationEntity.getUuid().hashCode(), intent, 0);

            // calculate notification time
            DateTime alarmTime = notificationEntity.getEventTime().toDateTime(DateTimeZone.UTC).plusMinutes(offsetInMinutes);

            Log.d("LocalNotificationsAlarmReceiver", "setAlarm to: " + alarmTime);
            // setRepeating
            if(Build.VERSION.SDK_INT >= 19 ) {
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, alarmTime.getMillis(), alarmIntent);
            } else {
                alarmMgr.set(AlarmManager.RTC_WAKEUP, alarmTime.getMillis(), alarmIntent);
            }
        }

        // TODO: persist notificationsToPersist
        new NotificationsPersister(context).add(notificationsToPersist);
    }
    // END_INCLUDE(set_alarm)

    public void setSyncAlarm(Context context) {
        Log.d("LocalNotificationsAlarmReceiver", "setSyncAlarm called");

        activateBootReceiver(context);


        Intent intent = new Intent(context, LocalNotificationsAlarmReceiver.class);
        intent.putExtra("alarmType", SYNC_TYPE);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(context,0,intent,0);

        long firstSync =  DateTime.now(DateTimeZone.UTC).plusSeconds(120).getMillis();
        long interval = 86400000/2; //half a day

        // Hopefully your alarm will have a lower frequency than this!
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, firstSync, interval, alarmIntent);
    }

    public void resetSyncAlarm(Context context) {
        Log.d("LocalNotificationsAlarmReceiver", "setSyncAlarm called");

        activateBootReceiver(context);


        Intent intent = new Intent(context, LocalNotificationsAlarmReceiver.class);
        intent.putExtra("alarmType", SYNC_TYPE);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(context,0,intent,0);

        long firstSync =  DateTime.now(DateTimeZone.UTC).plusMillis(86400000/2).getMillis();
        long interval = 86400000/2; //half a day

        // Hopefully your alarm will have a lower frequency than this!
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, firstSync, interval, alarmIntent);
    }

    private void activateBootReceiver(Context context) {
        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void deactivateBootReceiver(Context context) {
        // Disable {@code BootReceiver} so that it doesn't automatically restart the
        // alarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }


    /**
     * Cancels the alarm.
     * @param uuidHash
     */
    //BEGIN_INCLUDE(cancel_alarm)
    public void cancelAlarm(Context context, int uuidHash) {
        // get alarm manager
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        NotificationsPersister notificationsPersister = new NotificationsPersister(context);
        NotificationEntity notificationEntity = notificationsPersister.find(uuidHash);
        if(notificationEntity == null) {
            Log.e("LocalNotificationsAlarmReceiver", "tried to cancel inexistant NotificationEntity");
            return;
        }

        // recreate same alarmintent (extras can be ignored)
        PendingIntent alarmIntent = createSimplePendingIntent(context, LocalNotificationsAlarmReceiver.class, notificationEntity);

        // If the alarm has been set, cancel it.
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }

        notificationsPersister.removeOne(notificationEntity.getUuid().hashCode());


    }

    public void cancelAlarms(Context context, List<NotificationEntity> notificationEntityList) {
        // get alarm manager
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        // recreate same alarmintent (extras can be ignored)
        PendingIntent alarmIntent;
        for(NotificationEntity ne : notificationEntityList) {
            alarmIntent = createSimplePendingIntent(context, LocalNotificationsAlarmReceiver.class, ne);
            // If the alarm has been set, cancel it.
            if (alarmMgr!= null) {
                alarmMgr.cancel(alarmIntent);
            }
        }
    }

    public void cancelAllAlarms(Context context) {
        List<NotificationEntity> notificationEntities = new NotificationsPersister(context).retrieve();
        Log.d("LocalNotificationsAlarmReceiver", "cancelAllAlarms (Count: " + notificationEntities + ")");
        for(NotificationEntity ne : notificationEntities) {
            cancelAlarm(context, ne.getUuid().hashCode());
        }

    }
    //END_INCLUDE(cancel_alarm)

    private PendingIntent createSimplePendingIntent(Context context, Class<?> cls, NotificationEntity notificationEntity) {
        // create intent with uuid
        Intent intent = new Intent(context, LocalNotificationsAlarmReceiver.class);

        // create unique PendingIntent
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, notificationEntity.getUuid().hashCode(), intent, 0);

        return alarmIntent;
    }

    private boolean isSyncActive(Context context) {
        return new UserSettingsPersister(context).readNotificationActivated();
    }

}