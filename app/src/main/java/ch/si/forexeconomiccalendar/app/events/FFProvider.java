package ch.si.forexeconomiccalendar.app.events;

import android.content.Context;
import android.util.Log;

import org.joda.time.DateTime;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.si.forexeconomiccalendar.app.events.entities.FFEvent;
import ch.si.forexeconomiccalendar.app.events.importer.FFImporter;
import ch.si.forexeconomiccalendar.app.events.importer.FFParser;
import ch.si.forexeconomiccalendar.app.events.importer.ImportManager;
import ch.si.forexeconomiccalendar.app.exceptions.NetworkDownException;
import ch.si.forexeconomiccalendar.app.persistance.EventsPersister;
import ch.si.forexeconomiccalendar.app.events.enums.*;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;
import ch.si.forexeconomiccalendar.app.util.EconomicWeek;
import ch.si.forexeconomiccalendar.app.util.Week;

/**
 * Created by kensteiner on 26/07/14.
 */
public class FFProvider {
    private Context context;
    private ImportManager importManager;
    private EventsPersister<FFEvent> eventsPersister;

    public FFProvider(Context context) {
        this.context = context;
        this.importManager = new ImportManager(context);
        this.eventsPersister = new EventsPersister<FFEvent>(context);
    }

    public List<FFEvent> importWeek(Week week) throws NetworkDownException {
        long belongsToWeekId;
        switch (week) {
            case LAST_WEEK: belongsToWeekId = EconomicWeek.getLastWeekId(); break;
            case THIS_WEEK: belongsToWeekId = EconomicWeek.getCurrentWeekId(); break;
            case NEXT_WEEK: belongsToWeekId = EconomicWeek.getNextWeekId(); break;
            default: belongsToWeekId = EconomicWeek.getCurrentWeekId(); break;
        }
        Log.d("FFProvider", "starting import");
        // import data into document
        DateTime mondayOfWeek = new DateTime(belongsToWeekId);
        Document importedDocument = FFImporter.getEventsFromServer(week);

        // create economic events from document
        FFParser parser = new FFParser(importedDocument, belongsToWeekId);
        System.out.println("[FFProvider] providing " + parser.getFFEvents().size() + " Economic Events");
        Log.d("FFProvider","finished import");
        return parser.getFFEvents();
    }

    public void saveToFileStorage(List<FFEvent> ffEventList, Week week) {
        eventsPersister.storeEvents(ffEventList, week);
        importManager.setImported(week);
    }

    public List<FFEvent> loadFromFileStorage(Week week) {
        List<FFEvent> returnList = eventsPersister.retrieveStoredEvents(week);
        if (returnList == null) {
            returnList = new ArrayList<FFEvent>();
        }
        return returnList;
    }

    public List<FFEvent> provideAllFromWeek(Week week) throws NetworkDownException {
        if(importManager.needsImport(week)) {
            Log.d("FFProvider", "Up to date data not available. Importing...");
            List<FFEvent> importedFFEventList = importWeek(week);
            saveToFileStorage(importedFFEventList, week);
        }
        Log.d("FFProvider", "loading weeks data");
        List<FFEvent> eventsFromStorage = loadFromFileStorage(week);
        return filterEventsWithEventSettings(eventsFromStorage);
    }

    public List<FFEvent> provideAllFromWeekForceUpdate(Week week) throws NetworkDownException {
        List<FFEvent> importedFFEventList = importWeek(week);
        saveToFileStorage(importedFFEventList, week);
        Log.d("FFProvider", "loading weeks data");
        return filterEventsWithEventSettings(importedFFEventList);
    }

    public boolean needsImport(Week week) { return importManager.needsImport(week); }

    public List<FFEvent> provideAllFromWeekWithoutUpdate(Week week) {
        List<FFEvent> eventsFromStorage = loadFromFileStorage(week);
        return filterEventsWithEventSettings(eventsFromStorage);
    }

    public List<FFEvent> filterEventsWithEventSettings(List<FFEvent> inputList) {
        Log.d("FFProvider","filtering events");

        UserSettingsPersister userSettingsPersister = new UserSettingsPersister(context);
        Map<Impact,Boolean> impactSettingsMap = userSettingsPersister.readImpactSettings();

        // Filter by impact
        boolean showHighImpact = impactSettingsMap.get(Impact.HIGH);
        boolean showMediumImpact = impactSettingsMap.get(Impact.MEDIUM);
        boolean showLowImpact = impactSettingsMap.get(Impact.LOW);
        List<FFEvent> impactFilteredList = new ArrayList<FFEvent>();
        for (FFEvent ffe : inputList) {
            if ((ffe.getImpact() == Impact.HIGH) && (showHighImpact)){
                impactFilteredList.add(ffe);
                continue;
            } else if ((ffe.getImpact() == Impact.MEDIUM) && (showMediumImpact)) {
                impactFilteredList.add(ffe);
                continue;
            } else if ((ffe.getImpact() == Impact.LOW) && (showLowImpact)) {
                impactFilteredList.add(ffe);
                continue;
            }
        }

        Map<Currency,Boolean> currencySettingsMap = userSettingsPersister.readCurrencySettings();
        boolean showAUD = currencySettingsMap.get(Currency.AUD);
        boolean showCAD = currencySettingsMap.get(Currency.CAD);
        boolean showCHF = currencySettingsMap.get(Currency.CHF);
        boolean showCNY = currencySettingsMap.get(Currency.CNY);
        boolean showEUR = currencySettingsMap.get(Currency.EUR);
        boolean showGBP = currencySettingsMap.get(Currency.GBP);
        boolean showJPY = currencySettingsMap.get(Currency.JPY);
        boolean showNZD = currencySettingsMap.get(Currency.NZD);
        boolean showUSD = currencySettingsMap.get(Currency.USD);

        // Filter by currency
        List<FFEvent> currencyAndImpactFilteredList = new ArrayList<FFEvent>();
        for (FFEvent ffe : impactFilteredList) {
            switch (ffe.getCurrency()) {
                case AUD: if (showAUD) {currencyAndImpactFilteredList.add(ffe); } break;
                case CAD: if (showCAD) {currencyAndImpactFilteredList.add(ffe); } break;
                case CHF: if (showCHF) {currencyAndImpactFilteredList.add(ffe); } break;
                case CNY: if (showCNY) {currencyAndImpactFilteredList.add(ffe); } break;
                case EUR: if (showEUR) {currencyAndImpactFilteredList.add(ffe); } break;
                case GBP: if (showGBP) {currencyAndImpactFilteredList.add(ffe); } break;
                case JPY: if (showJPY) {currencyAndImpactFilteredList.add(ffe); } break;
                case NZD: if (showNZD) {currencyAndImpactFilteredList.add(ffe); } break;
                case USD: if (showUSD) {currencyAndImpactFilteredList.add(ffe); } break;
            }
        }
        return currencyAndImpactFilteredList;
    }

    public void refresh() throws NetworkDownException {
        Log.d("FFProvider","starting refresh check");
        for(Week week : Week.values()) {
            if(importManager.needsRefresh(week)) {
                Log.d("FFProvider", "refreshing " + week.toString());
                saveToFileStorage(importWeek(week), week);
            }
        }
    }
}
