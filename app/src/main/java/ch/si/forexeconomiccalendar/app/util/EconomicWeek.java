package ch.si.forexeconomiccalendar.app.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;

public class EconomicWeek {
    private static final long ONE_WEEK = 604800000;

    public static long getLastWeekId() {
        return getPreviousSunday().getMillis() - ONE_WEEK;
    }

    public static long getCurrentWeekId(){
        return getPreviousSunday().getMillis();
    }

    public static long getNextWeekId(){
        return getPreviousSunday().getMillis() + ONE_WEEK;
    }

    public static DateTime getNextSunday(){
        return getPreviousSunday().plusDays(7);
    }
	
	public static DateTime getPreviousSunday() {
		DateTime dateTime = DateTime.now().withZone(DateTimeZone.forID("America/New_York"));
        if (dateTime.getDayOfWeek() != DateTimeConstants.SUNDAY) {
            dateTime = dateTime.withDayOfWeek(DateTimeConstants.MONDAY).minusDays(1);
        }
		dateTime = dateTime.minusMinutes(dateTime.getMinuteOfHour());
		dateTime = dateTime.minusHours(dateTime.getHourOfDay());
		dateTime = dateTime.minusSeconds(dateTime.getSecondOfMinute());
		dateTime = dateTime.minusMillis(dateTime.getMillisOfSecond());
		return dateTime;
	}
}
