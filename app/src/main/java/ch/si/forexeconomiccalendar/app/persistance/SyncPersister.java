package ch.si.forexeconomiccalendar.app.persistance;

import android.content.Context;
import android.content.SharedPreferences;

public class SyncPersister {
	private final String syncSettings ="TEST00_SYNC_SETTINGS";
	private final String syncHistory ="TEST03_SYNC_HISTORY";
	private final String preferenceFilekey = "com.example.testaccesssharedpreferences.PREFERENCE_FILE_KEY";
	
	private SharedPreferences sharedPreferences;
	
	public SyncPersister(Context context){
		this.sharedPreferences = context.getSharedPreferences(preferenceFilekey, Context.MODE_PRIVATE);
	}

	public String readSyncSettings(){
		String readValue = sharedPreferences.getString(syncSettings, "{}");
		System.out.println(readValue);
		return readValue;
	}

	public void writeSyncSettings(String inputString){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(syncSettings, inputString);
		editor.commit();
	}

	public String readSyncHistory(){
		String readValue = sharedPreferences.getString(syncHistory, "{\"sync_history\":[]}");
		System.out.println(readValue);
		return readValue;
	}

	public void writeSyncHistory(String inputString){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(syncHistory, inputString);
		editor.commit();
	}
}
