package ch.si.forexeconomiccalendar.app.events.importer;

import android.content.Context;
import android.content.SharedPreferences;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import ch.si.forexeconomiccalendar.app.util.EconomicWeek;
import ch.si.forexeconomiccalendar.app.util.Week;

/**
 * Created by kensteiner on 29/06/14.
 */
public class ImportManager {
    private final String IMPORT_HISTORY = "ch.si.forexeconomiccalendar.import_history";

    private final String LAST_WEEK = "IMPORT_LAST_WEEK";
    private final String THIS_WEEK = "IMPORT_THIS_WEEK";
    private final String NEXT_WEEK = "IMPORT_NEXT_WEEK";

    private SharedPreferences sharedPreferences;

    public ImportManager(Context context) {
        this.sharedPreferences = context.getSharedPreferences(IMPORT_HISTORY, Context.MODE_PRIVATE);
    }

    public boolean needsImport(Week week) {
        String chosen_week = "";
        switch(week) {
            case LAST_WEEK: chosen_week = LAST_WEEK; break;
            case THIS_WEEK: chosen_week = THIS_WEEK; break;
            case NEXT_WEEK: chosen_week = NEXT_WEEK; break;
        }
        long lastImport = sharedPreferences.getLong(chosen_week, new DateTime().minusYears(10).getMillis());

        long previousMonday = EconomicWeek.getPreviousSunday().getMillis();
        if(lastImport < previousMonday) {
            return true;
        }
        return false;
    }

    public boolean needsRefresh(Week week) {
        String chosen_week = "";
        switch(week) {
            case LAST_WEEK: chosen_week = LAST_WEEK; break;
            case THIS_WEEK: chosen_week = THIS_WEEK; break;
            case NEXT_WEEK: chosen_week = NEXT_WEEK; break;
        }
        long lastImport = sharedPreferences.getLong(chosen_week, new DateTime().minusYears(10).getMillis());

        long timeTenMinutesAgo = DateTime.now().minusMinutes(10).getMillis();
        if(lastImport < timeTenMinutesAgo) {
            return true;
        }
        return false;

    }

    public void setImported(Week week) {
        String chosen_week = "";
        switch(week) {
            case LAST_WEEK: chosen_week = LAST_WEEK; break;
            case THIS_WEEK: chosen_week = THIS_WEEK; break;
            case NEXT_WEEK: chosen_week = NEXT_WEEK; break;
        }

        long syncTime = DateTime.now(DateTimeZone.forID("America/New_York")).getMillis();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(chosen_week,syncTime);
        editor.commit();
    }
}
