package ch.si.forexeconomiccalendar.app.events.entities;

import android.util.Log;

import org.jsoup.nodes.Document;

import java.io.Serializable;
import java.util.List;

import ch.si.forexeconomiccalendar.app.events.importer.FFDetailsParser;
import ch.si.forexeconomiccalendar.app.events.importer.FFImporter;
import ch.si.forexeconomiccalendar.app.events.importer.FFParser;
import ch.si.forexeconomiccalendar.app.exceptions.NetworkDownException;

/**
 * Created by kensteiner on 19/07/14.
 */
public class FFDetail implements Serializable {
    private int id;
    private List<FFSpec> specsList;
    private List<FFHistoryEntry> historyList;

    public FFDetail(int id, List<FFSpec> specsList, List<FFHistoryEntry> historyList) {
        this.id = id;
        this.specsList = specsList;
        this.historyList = historyList;
    }

    public int getId() {
        return id;
    }

    public List<FFSpec> getSpecsList() {
        return specsList;
    }

    public List<FFHistoryEntry> getHistoryList() {
        return historyList;
    }

    public static FFDetail importAndBuild(int id) throws NetworkDownException {
        Log.d("FFDetails", "importing detail id: " + id);
        Document doc = FFImporter.getDetailsFromServer(id);
        FFDetailsParser dp = new FFDetailsParser(doc);
        long startTime = System.currentTimeMillis();
        List<FFSpec> specs = dp.getSpecs();
        List<FFHistoryEntry> history = dp.getHistory();
        long duration = System.currentTimeMillis() - startTime;
        Log.d("FFDetail", "detail extraction duration in ms: " + duration);
        return new FFDetail(id, specs, history);
    }
}
