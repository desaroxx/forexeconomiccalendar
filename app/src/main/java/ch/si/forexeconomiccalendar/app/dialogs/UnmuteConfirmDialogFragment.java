package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 08/09/14.
 */
public class UnmuteConfirmDialogFragment extends DialogFragment {
    private UserSettingsPersister usp;

    public interface UnmuteConfirmDialogListener {
        public void onUpdateUnmuteConfirm();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        usp = new UserSettingsPersister(getActivity().getApplicationContext());
        usp.updateNotificationSoundDeactivatedUntil(-1L);
        usp.updateNotificationSoundActivated(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Successfully unmuted!");
        builder.setMessage("All alerts will come with a simple beep sound.");
        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                UnmuteConfirmDialogListener activity;
                try {
                    activity = (UnmuteConfirmDialogListener)getActivity();
                    activity.onUpdateUnmuteConfirm();
                } catch (ClassCastException e) {
                    // do nothing
                }
            }
        });

        return builder.create();
    }

}
