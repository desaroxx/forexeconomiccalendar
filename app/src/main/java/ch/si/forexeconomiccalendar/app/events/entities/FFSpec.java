package ch.si.forexeconomiccalendar.app.events.entities;

import java.io.Serializable;

/**
 * Created by kensteiner on 19/07/14.
 */
public class FFSpec implements Serializable {
    private String title;
    private String content;

    public FFSpec(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
