package ch.si.forexeconomiccalendar.app.lists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.events.entities.FFEvent;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by kensteiner on 31/05/14.
 */
public class CustomArrayAdapter extends ArrayAdapter<FFEvent> implements StickyListHeadersAdapter {

    private LayoutInflater inflater;
    private Context context;
    private FFEvent[] events;

    public CustomArrayAdapter(Context context, int resource, FFEvent[] objects) {
        super(context, resource, objects);
        System.out.print("[CustomArrayAdapter] init");
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.events = objects;
    }

    /* private view holder class */
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtPerformance;
        TextView txtTime;
        TextView txtCurrency;
    }

    private class HeaderViewHolder {
        TextView txtMenuName;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        FFEvent rowItem = getItem(position);

        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.custom_content_row, parent, false);

            holder.imageView = (ImageView) convertView.findViewById(R.id.status_color);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.txtPerformance = (TextView) convertView.findViewById(R.id.source);
            holder.txtTime = (TextView) convertView.findViewById(R.id.time);
            holder.txtCurrency = (TextView) convertView.findViewById(R.id.time_cst);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imageView.setImageResource(R.drawable.rectangle_neutral);
        switch (rowItem.getImpact()) {
            case HIGH: holder.imageView.setImageResource(R.drawable.rectangle_red); break;
            case MEDIUM: holder.imageView.setImageResource(R.drawable.rectangle_orange); break;
            case LOW: holder.imageView.setImageResource(R.drawable.rectangle_yellow); break;
            default: holder.imageView.setImageResource(R.drawable.rectangle_neutral); break;
        }


        holder.txtTitle.setText(rowItem.getTitle());

        DateTime eventTime = rowItem.getTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
        String eventTimeLocalFormatted = fmt.withZone(DateTimeZone.getDefault()).print(eventTime);
        holder.txtTime.setText(eventTimeLocalFormatted);


        holder.txtCurrency.setText(rowItem.getCurrency().toString());

        String actual = rowItem.getActual().getValue();
        if (actual.length() < 1) {
            actual = "---";
        }
        if (actual.length() < 6) {
            actual = String.format("%-10s", actual);
        }

        String forecast = rowItem.getForecast().getValue();
        if (forecast.length() < 1) {
            forecast = "---";
        }
        if (forecast.length() < 6) {
            forecast = String.format("%-6s", forecast);
        }

        String previous = rowItem.getPrevious().getValue();
        if (previous.length() < 1) {
            previous = "---";
        }
        if (previous.length() < 6) {
            previous = String.format("%-10s", previous);
        }

        holder.txtPerformance.setText("Act: " + actual + " Fore: " + forecast + " Prev: " + previous);

        return convertView;

    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        FFEvent rowItem = getItem(position);

        HeaderViewHolder holder;
        if(convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.custom_header_row, parent, false);

            holder.txtMenuName = (TextView) convertView.findViewById(R.id.header_name);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        String formattedDate = rowItem.getTime().toDateTime(DateTimeZone.getDefault()).toString(fmt);
        int dayOfWeek = rowItem.getTime().getDayOfWeek();
        String dayName = "";
        switch (dayOfWeek) {
            case 0: dayName = "Sunday"; break;
            case 1: dayName = "Monday"; break;
            case 2: dayName = "Tuesday"; break;
            case 3: dayName = "Wednesday"; break;
            case 4: dayName = "Thursday"; break;
            case 5: dayName = "Friday"; break;
            case 6: dayName = "Saturday"; break;
            case 7: dayName = "Sunday"; break;
        }
        holder.txtMenuName.setText(dayName + ", " + formattedDate);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMddyyyy");
        DateTime time = events[position].getTime().toDateTime(DateTimeZone.getDefault());
        return new Long(time.toString(fmt));
    }

    public FFEvent getEvent(int position) {
        return events[position];
    }
}