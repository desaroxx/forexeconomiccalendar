package ch.si.forexeconomiccalendar.app.alarms;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by kensteiner on 30/06/14.
 */
public class NotificationEntity implements Serializable {
    private DateTime eventTime;
    private String title;
    private UUID uuid;

    public NotificationEntity(DateTime eventTime, String title) {
        this.eventTime = eventTime;
        this.title = title;
        this.uuid = UUID.randomUUID();
    }

    public DateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(DateTime eventTime) {
        this.eventTime = eventTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
