package ch.si.forexeconomiccalendar.app;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;


import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.alarms.NotificationsManager;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 07/07/14.
 */
public class GlobalApplication extends Application {

    private static final String PROPERTY_ID = "UA-52586818-2";

    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER,
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public GlobalApplication() {
        super();
    }

    @Override
    public void onCreate() {

        // check old versions settings
        UserSettingsPersister usp = new UserSettingsPersister(getApplicationContext());
        if (usp.readNotificationTime() == -1000) {
            usp.writeNotificationActivated(false);
            usp.updateNotificationTime(-5);
        }


        super.onCreate();

        Log.d("GlobalApplication", "onCreate");

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                new NotificationsManager(getApplicationContext()).createNotifications();
                return null;
            }
        }.execute();
        initSinletons();

        Tracker t = getTracker(TrackerName.APP_TRACKER);

        // set uid if available
        PackageManager pm = getPackageManager();
        if (pm.checkPermission(Manifest.permission.GET_ACCOUNTS, getPackageName()) == PackageManager.PERMISSION_GRANTED) {
            String username = getUsername();
            if (username != null) {
                Log.d("GlobalApplication", "uid:" + username.hashCode() + ", uname: " + username);
                t.set("&uid", "" + username.hashCode());
            }
        }

        t.send(new HitBuilders.EventBuilder().setAction("appStarted").build());
    }

    protected void initSinletons() {
        MySingleton.initInstance();
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = analytics.newTracker(PROPERTY_ID);
//            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
//                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
//                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    private String getUsername() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            possibleEmails.add(account.name);
        }

        if(!possibleEmails.isEmpty() && possibleEmails.get(0) != null){
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");
            if(parts.length > 0 && parts[0] != null)
                return parts[0];
            else
                return null;
        }else
            return null;
    }

}
