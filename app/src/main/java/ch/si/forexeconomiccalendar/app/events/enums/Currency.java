package ch.si.forexeconomiccalendar.app.events.enums;

/**
 * Created by kensteiner on 17/07/14.
 */
public enum Currency {
    AUD,
    CAD,
    CHF,
    CNY,
    EUR,
    GBP,
    JPY,
    NZD,
    USD
}
