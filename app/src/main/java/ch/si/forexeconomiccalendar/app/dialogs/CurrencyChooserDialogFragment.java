package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import ch.si.forexeconomiccalendar.app.events.enums.Currency;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 03/08/14.
 */
public class CurrencyChooserDialogFragment extends DialogFragment{
    private UserSettingsPersister usp;

    private Set<Currency> selectedCurrencySet;
    private Map<Currency,Boolean> savedCurrencySettings;

    public interface CurrencyChooserDialogListener {
        public void onUpdateCurrencyChooser();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        usp = new UserSettingsPersister(getActivity().getApplicationContext());
        savedCurrencySettings = usp.readCurrencySettings();

        selectedCurrencySet = new HashSet<Currency>();
        final String[] currencies = new String[savedCurrencySettings.size()];
        boolean[] activeCurrencies = new boolean[savedCurrencySettings.size()];
        int counter = 0;
        for (Map.Entry<Currency,Boolean> entry : savedCurrencySettings.entrySet()) {
            currencies[counter] = entry.getKey().toString();
            boolean isActive = entry.getValue();
            activeCurrencies[counter] = isActive;
            if (isActive) {
                selectedCurrencySet.add(entry.getKey());
            }
            counter++;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select currencies:");
        builder.setMultiChoiceItems(currencies, activeCurrencies, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                Currency chosenCurrency = Currency.valueOf(currencies[which]);
                if (isChecked) {
                    selectedCurrencySet.add(chosenCurrency);
                } else if (selectedCurrencySet.contains(chosenCurrency)) {
                    selectedCurrencySet.remove(chosenCurrency);
                }
            }
        });

        builder.setPositiveButton(Html.fromHtml("<b>Save</b>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Map<Currency,Boolean> map = new TreeMap<Currency,Boolean>();
                for (Map.Entry<Currency,Boolean> entry : savedCurrencySettings.entrySet()) {
                    Currency tempCurrency = entry.getKey();
                    map.put(tempCurrency, selectedCurrencySet.contains(tempCurrency));
                }
                usp.updateCurrencySettings(map);

                dialog.dismiss();
                CurrencyChooserDialogListener activity;
                try {
                    activity = (CurrencyChooserDialogListener)getActivity();
                    activity.onUpdateCurrencyChooser();
                } catch (ClassCastException e) {
                    // do nothing
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }
}
