package ch.si.forexeconomiccalendar.app.alarms;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.joda.time.DateTime;

import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.activities.MainActivity;
import ch.si.forexeconomiccalendar.app.activities.UserSettingsActivity;
import ch.si.forexeconomiccalendar.app.persistance.NotificationsPersister;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;


/**
 * Created by kensteiner on 30/06/14.
 */
public class SchedulingService extends IntentService {
    private NotificationManager mNotificationManager;

    private final int SYNC_TYPE = 0;
    private final int NOTIFICATION_TYPE = 1;

    public SchedulingService() {
        super("SchedulingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // decide if: SYNC_TYPE or NOTIFICATION_TYPE
        Bundle bundle = intent.getExtras();
        int alarmType = bundle.getInt("alarmType");
        if(alarmType == SYNC_TYPE) {
            // handle SYNC_TYPE
            Log.d("SchedulingService", "onHandleIntent: SYNC_TYPE");
            syncAlarms();

        } else {
            // handle NOTIFICATION_TYPE
            Log.d("SchedulingService", "onHandleIntent: NOTIFICATION_TYPE");
            int uuidHash = 0;
            if(bundle.containsKey("uuid-hash")) {
                uuidHash = bundle.getInt("uuid-hash");
            } else {
                Log.e("LocalNotificationsAlarmReceiver", "bundle didn't receive uuid");
            }
            sendNotification(uuidHash);

        }

    }

    private void syncAlarms() {
        Log.d("SchedulingService", "syncAlarms");
        new NotificationsManager(getApplicationContext()).updateNotifications();

    }

    private void sendNotification(int uuidHash) {
        Log.d("SchedulingService", "sendNotification");

        NotificationsPersister notificationsPersister = new NotificationsPersister(getApplicationContext());
        NotificationEntity notificationEntity = notificationsPersister.find(uuidHash);

        // discard if failed to find notificationEntitiy
        if(notificationEntity == null) {
            return;
        }

        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Reminder: " + notificationEntity.getTitle())
                .setAutoCancel(true)
                .setOnlyAlertOnce(true);

        UserSettingsPersister usp = new UserSettingsPersister(getApplicationContext());



        // check if alert sound is enabled
        boolean doAlert;
        long timeNow = DateTime.now().getMillis();
        boolean stillDeactivated = (usp.readNotificationSoundDeactivatedUntil() - timeNow) + 2L > 0L;

        if (stillDeactivated) {
            doAlert = false;
        } else {
            doAlert = usp.readNotificationSoundActivated();
        }
        // attach sound if supposed to make sound
        if (doAlert) {
            mBuilder.setSound(alarmSound);
        }

        // add buttons to notification --> supported only Android API 16 and higher
        if (Build.VERSION.SDK_INT >= 16) {
            // add mute / unmute button
            if (doAlert) {
                Intent muteIntent = new Intent(this, UserSettingsActivity.class);
                muteIntent.putExtra("interest", "mute");
                muteIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                PendingIntent mutePendingIntent = PendingIntent.getActivity(this, 10, muteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.addAction(R.drawable.ic_mute_notification, "Mute", mutePendingIntent);
            } else {
                Intent unmuteIntent = new Intent(this, UserSettingsActivity.class);
                unmuteIntent.putExtra("interest", "unmute");
                unmuteIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent unmutePendingIntent = PendingIntent.getActivity(this, 11, unmuteIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.addAction(R.drawable.ic_unmute_notification, "Unmute", unmutePendingIntent);
            }

            // add settings button
            Intent openSettingsIntent = new Intent(this, UserSettingsActivity.class);
            openSettingsIntent.putExtra("interest", "edit_settings");
            openSettingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent openSettingsPendingIntent = PendingIntent.getActivity(this, 12, openSettingsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.addAction(R.drawable.ic_settings_notification, "Settings", openSettingsPendingIntent);
        }

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.cancel(1);
        mNotificationManager.notify(1, mBuilder.build());

        Log.d("SchedulingService", "removeOne from notificationsPersister: uuidHash" + uuidHash);
        notificationsPersister.removeOne(uuidHash);

    }
}
