package ch.si.forexeconomiccalendar.app.events.enums;

/**
 * Created by kensteiner on 19/07/14.
 */
public enum Performance {
    WORSE,
    EXPECTED,
    BETTER
}
