package ch.si.forexeconomiccalendar.app.events.enums;

/**
 * Created by kensteiner on 18/07/14.
 */
public enum Weekday {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY
}
