package ch.si.forexeconomiccalendar.app.util.coloredstring;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import java.util.List;
import java.util.Map;

import ch.si.forexeconomiccalendar.app.events.enums.Impact;
import ch.si.forexeconomiccalendar.app.util.EventCategory;

/**
 * Created by kensteiner on 29/08/14.
 */
public class SpannableBuilder {

    public static Spannable createColoredBlock(Impact impact) {
        Spannable spannable = new SpannableString("▌");
        switch (impact) {
            case HIGH:
                spannable.setSpan(new ForegroundColorSpan(0xffe0162b), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                break;
            case MEDIUM:
                spannable.setSpan(new ForegroundColorSpan(0xffe06c1b), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                break;
            case LOW:
                spannable.setSpan(new ForegroundColorSpan(0xffd3d800), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                break;
        }
        return spannable;
    }

    public static Spanned createImpact(Impact impact) {
        switch (impact) {
            case HIGH: return (Spanned) TextUtils.concat(createColoredBlock(Impact.HIGH), " ", "HIGH");
            case MEDIUM: return (Spanned) TextUtils.concat(createColoredBlock(Impact.MEDIUM), " ", "MEDIUM");
            case LOW: return (Spanned) TextUtils.concat(createColoredBlock(Impact.LOW), " ", "LOW");
            default: return (Spanned) TextUtils.concat(createColoredBlock(Impact.HIGH), " ", "HIGH");
        }
    }

    public static Spanned createColoredImpacts(Map<Impact,Boolean> impactMap) {

        Spanned spanned = new SpannedString("");

        boolean atLeastOneActiveImpact = false;

        int activeImpactsCount = 0;
        for (Map.Entry<Impact,Boolean> entry : impactMap.entrySet()) {
            if (entry.getValue() == true) {
                ++activeImpactsCount;
            }
        }

        int separationCommasAvailable = activeImpactsCount - 1;

        if (impactMap.get(Impact.HIGH)) {
            String separator = "";
            if (separationCommasAvailable > 0) {
                separator = ", ";
                --separationCommasAvailable;
            }
            spanned = (Spanned) TextUtils.concat(spanned, createColoredBlock(Impact.HIGH), " ", "HIGH", separator);
            atLeastOneActiveImpact = true;
        }
        if (impactMap.get(Impact.MEDIUM)) {
            String separator = "";
            if (separationCommasAvailable > 0) {
                separator = ", ";
                --separationCommasAvailable;
            }
            spanned = (Spanned) TextUtils.concat(spanned, createColoredBlock(Impact.MEDIUM), " ", "MEDIUM", separator);
            atLeastOneActiveImpact = true;
        }
        if (impactMap.get(Impact.LOW)) {
            String separator = "";
            if (separationCommasAvailable > 0) {
                separator = ", ";
                --separationCommasAvailable;
            }
            spanned = (Spanned) TextUtils.concat(spanned, createColoredBlock(Impact.LOW), " ", "LOW", separator);
            atLeastOneActiveImpact = true;
        }

        if (!atLeastOneActiveImpact) {
            spanned = new SpannableString(" (none selected)");
        }
        //return (Spanned) spanned.subSequence(0 , spanned.length() - 2);
        return spanned;
    }

}
