package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import org.joda.time.DateTime;

import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 08/09/14.
 */
public class MuteSoundChooserDialogFragment extends DialogFragment {
    private UserSettingsPersister usp;

    private String muteOptions[] = {"Forever", "1 Hour", "4 Hours", "8 Hours", "24 Hours"};
    private long muteTime[] = {-1L, 60L, 4*60L, 8*60L, 24*60L};


    public interface MuteSoundChooserDialogListener {
        public void onUpdateMuteSoundChooser();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        usp = new UserSettingsPersister(getActivity().getApplicationContext());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Mute for how long?");
        builder.setItems(muteOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    usp.updateNotificationSoundActivated(false);
                    usp.updateNotificationSoundDeactivatedUntil(-1L);
                } else {
                    usp.updateNotificationSoundActivated(true);
                    long muteUntil = DateTime.now().getMillis() + (muteTime[which]*60L*1000L);
                    usp.updateNotificationSoundDeactivatedUntil(muteUntil);
                }

                dialog.dismiss();
                MuteSoundChooserDialogListener activity;
                try {
                    activity = (MuteSoundChooserDialogListener)getActivity();
                    activity.onUpdateMuteSoundChooser();
                } catch (ClassCastException e) {
                    // do nothing
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
