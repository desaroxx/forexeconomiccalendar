package ch.si.forexeconomiccalendar.app.util;

/**
 * Created by kensteiner on 29/06/14.
 */
public enum EventCategory {
    MMI,
    MEA,
    OKI,
    DEF;

}
