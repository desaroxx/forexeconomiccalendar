
package ch.si.forexeconomiccalendar.app.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.res.Configuration;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.gms.analytics.GoogleAnalytics;

import org.joda.time.DateTime;

import ch.si.forexeconomiccalendar.app.alarms.LocalNotificationsAlarmReceiver;
import ch.si.forexeconomiccalendar.app.alarms.NotificationEntity;
import ch.si.forexeconomiccalendar.app.alarms.NotificationsManager;
import ch.si.forexeconomiccalendar.app.dialogs.FirstStartAlertSettingsFragment;
import ch.si.forexeconomiccalendar.app.fragments.EventsSectionFragment;
import ch.si.forexeconomiccalendar.app.fragments.EventsTabContainerFragment;
import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.fragments.WebViewFragment;
import ch.si.forexeconomiccalendar.app.inappbilling.IabHelper;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;


public class MainActivity extends FragmentActivity {

	private static final String TAG = MainActivity.class.getSimpleName();
	
//	private DrawerLayout mDrawerLayout;
//	private ListView mDrawerList;
//	private ActionBarDrawerToggle mDrawerToggle;
//
//	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
//	private String[] mDrawerItmes;

    private FragmentTabHost mTabHost;

    // in app billing
    IabHelper mHelper;
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn6S2WBda2JsxeU4kg3pzVykcbw0AKT6YgJNbB/z+MwxrxXE+pZfdNv1eeHnEK9Tvi807CI43iHVUrcdjj+oDS9My9saTJX4n7rNSrB93KCLtRQdjj0BsPwUYbAjuVbEx2EJE0K/Z8OObg5cCe1P0KWl4xYjqIetCt7b0CPmDNZisL8gQeGSLoQ0NGZgbhPc/gEmI8lb9Ou+pOAO/j2cwbd9AfJ7tl76UYx2XwOWX50NOCot6rRI7voJfESJS6GmzefEdSJ06zH9SbCaNpBh3VeoCnY13nixvqsjs+C+kUpYOUzOemA4E+oXMCWFedLYogvulJREnDsTfwEujv4K1iwIDAQAB";
    static final String PREMIUM_SKU = "test_premium_account";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        // first application start
        if (new UserSettingsPersister(getApplicationContext()).readIsFirstLaunch()) {
            firstApplicationStart();
        }

//        // TODO: remove for release
//        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(getApplicationContext());
//        googleAnalytics.setAppOptOut(true);

//		mTitle = mDrawerTitle = getTitle();
//
//		mDrawerItmes = getResources().getStringArray(R.array.drawer_titles);
//		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//		mDrawerList = (ListView) findViewById(R.id.left_drawer);
//
//		// set a custom shadow that overlays the main content when the drawer oepns
//		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,  GravityCompat.START);
//
//		// Add items to the ListView
//		mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mDrawerItmes));
//		// Set the OnItemClickListener so something happens when a
//		// user clicks on an item.
//		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
//
//		// Enable ActionBar app icon to behave as action to toggle nav drawer
//		getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().setHomeButtonEnabled(true);
//
//		mDrawerToggle = new ActionBarDrawerToggle(
//				this,
//				mDrawerLayout,
//				R.drawable.ic_drawer,
//				R.string.drawer_open,
//				R.string.drawer_close
//				) {
//			public void onDrawerClosed(View view) {
//				getActionBar().setTitle(mTitle);
//				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
//			}
//
//			public void onDrawerOpened(View drawerView) {
//				getActionBar().setTitle(mDrawerTitle);
//				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
//			}
//		};
//
//		mDrawerLayout.setDrawerListener(mDrawerToggle);
//
//		// Set the default content area to item 0
//		// when the app opens for the first time
//		if(savedInstanceState == null) {
//			navigateTo(0);
//		}
        navigateTo(0);

        mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(),R.id.realtabcontent);

        Bundle lastWeekBundle = new Bundle();
        lastWeekBundle.putString("TARGET_WEEK", "LAST_WEEK");
        mTabHost.addTab(mTabHost.newTabSpec("LAST WEEK").setIndicator("LAST WEEK"), EventsSectionFragment.class, lastWeekBundle);

        Bundle thisWeekBundle = new Bundle();
        thisWeekBundle.putString("TARGET_WEEK", "THIS_WEEK");
        mTabHost.addTab(mTabHost.newTabSpec("THIS WEEK").setIndicator("THIS WEEK"), EventsSectionFragment.class, thisWeekBundle);

        Bundle nextWeekBundle = new Bundle();
        nextWeekBundle.putString("TARGET_WEEK", "NEXT_WEEK");
        mTabHost.addTab(mTabHost.newTabSpec("NEXT WEEK").setIndicator("NEXT WEEK"), EventsSectionFragment.class, nextWeekBundle);

        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_indicator_ab_example);
        mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_indicator_ab_example);
        mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.tab_indicator_ab_example);
        mTabHost.getTabWidget().getChildAt(0).getLayoutParams().height=65;
        mTabHost.getTabWidget().getChildAt(1).getLayoutParams().height=65;
        mTabHost.getTabWidget().getChildAt(2).getLayoutParams().height=65;
        mTabHost.setHorizontalScrollBarEnabled(true);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);
        Log.d("TabHost", "height: " + mTabHost.getHeight() + "; widget height: " + mTabHost.getTabWidget().getChildAt(0).getHeight());

        mTabHost.getHeight();


        mTabHost.setCurrentTab(1);

        setTitle("Events");

        // in app billing

//        mHelper = new IabHelper(this, base64EncodedPublicKey);
//
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//            @Override
//            public void onIabSetupFinished(IabResult result) {
//                if (!result.isSuccess()) {
//                    Log.d(TAG, "In-app Billing setup failed: " +
//                            result);
//                } else {
//                    Log.d(TAG, "In-app Billing is set up OK");
//                }
//            }
//        });

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//	/*
//	 * If you do not have any menus, you still need this function
//	 * in order to open or close the NavigationDrawer when the user
//	 * clicking the ActionBar app icon.
//	 */
//
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
//		if(mDrawerToggle.onOptionsItemSelected(item)) {
//			return true;
//		}
		if(item.getItemId() == R.id.filter_settings) {
            startActivity(new Intent(this, UserSettingsActivity.class));
        }
		return super.onOptionsItemSelected(item);
	}
	
	/*
	 * When using the ActionBarDrawerToggle, you must call it during onPostCreate()
	 * and onConfigurationChanged()
	 */
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
//		mDrawerToggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
//		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	private class DrawerItemClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Log.v(TAG, "ponies");
			navigateTo(position);
		}
	}
	
	private void navigateTo(int position) {
		Log.v(TAG, "List View Item: " + position);
		
		switch(position) {
            case 0:
                getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame,
                            EventsTabContainerFragment.newInstance(),
                            EventsTabContainerFragment.TAG).commit();
                break;
            case 1:
                getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame,
                            WebViewFragment.newInstance(),
                            WebViewFragment.TAG).commit();
                break;
            case 2:
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        new NotificationsManager(getApplicationContext()).createNotifications();
                        return null;
                    }
                }.execute();
                break;
            case 3:
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        new NotificationsManager(getApplicationContext()).printQueuedNotifications();
                        DateTime time = new DateTime(new UserSettingsPersister(getApplicationContext()).readBootReceiverLastCall());
                        Log.d("MainActivitiy", "BootReceiver last update:" + time);

                        NotificationEntity ne1 = new NotificationEntity(DateTime.now().plusSeconds(5),"UBS Research Papers1");
                        NotificationEntity ne2 = new NotificationEntity(DateTime.now().plusSeconds(10),"UBS Research Papers2");
                        NotificationEntity ne3 = new NotificationEntity(DateTime.now().plusSeconds(15),"UBS Research Papers3");
                        NotificationEntity ne4 = new NotificationEntity(DateTime.now().plusSeconds(20),"UBS Research Papers4");
                        NotificationEntity ne5 = new NotificationEntity(DateTime.now().plusSeconds(25),"UBS Research Papers5");

                        new LocalNotificationsAlarmReceiver().setAlarm(getApplicationContext(), ne1);
                        new LocalNotificationsAlarmReceiver().setAlarm(getApplicationContext(), ne2);
                        new LocalNotificationsAlarmReceiver().setAlarm(getApplicationContext(), ne3);
                        new LocalNotificationsAlarmReceiver().setAlarm(getApplicationContext(), ne4);
                        new LocalNotificationsAlarmReceiver().setAlarm(getApplicationContext(), ne5);
                        return null;
                    }
                }.execute();
                break;
            case 4:
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        new LocalNotificationsAlarmReceiver().cancelAllAlarms(getApplicationContext());
                        return null;
                    }
                }.execute();
                break;
            case 5:
                startActivity(new Intent(this, HorizontalScroll.class));
                break;
//            case 6:
//                // in-app billing
//                buy();
//                break;
//            case 7:
//                List additionalSKUList = new ArrayList();
//                additionalSKUList.add(PREMIUM_SKU);
//                mHelper.queryInventoryAsync(true, additionalSKUList, mQueryFinishedListener);
//                break;
//            case 8:
//
//                break;
		}
//        mDrawerLayout.closeDrawer(mDrawerList);
	}
	
	@Override
	public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);

    }

    private void firstApplicationStart() {
        FirstStartAlertSettingsFragment fsasf = FirstStartAlertSettingsFragment.newInstance();
        fsasf.show(getFragmentManager(), "");
    }

}
