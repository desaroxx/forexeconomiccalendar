package ch.si.forexeconomiccalendar.app.persistance.adapters;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by kensteiner on 26/07/14.
 */
public class InternalStoragePersister {

    public static void write(String FILE_NAME, Object object, Context context) {
        try {
            FileOutputStream fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream of = new ObjectOutputStream(fos);
            of.writeObject(object);
            of.flush();
            of.close();
            fos.close();
        } catch (Exception e) {
            Log.e("InternalStorage", e.getMessage());
        }
    }

    public static Object read(String FILE_NAME, Context context) {
        Object object = null;
        try {
            FileInputStream fis = context.openFileInput(FILE_NAME);
            ObjectInputStream oi = new ObjectInputStream(fis);
            object = oi.readObject();
            oi.close();
        } catch (Exception e) {
            Log.e("InternalStorage", e.getMessage());
        }
        return object;
    }
}
