package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import ch.si.forexeconomiccalendar.app.events.enums.Impact;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 03/08/14.
 */
public class ImpactChooserDialogFragment extends DialogFragment {
    private UserSettingsPersister usp;

    private Set<Impact> selectedImpactSet;
    private Map<Impact,Boolean> savedImpactSettings;

    public interface ImpactChooserDialogListener {
        public void onUpdateImpactChooser();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        usp = new UserSettingsPersister(getActivity().getApplicationContext());
        savedImpactSettings = usp.readImpactSettings();

        selectedImpactSet = new HashSet<Impact>();
        final String[] impacts = new String[savedImpactSettings.size()];
        boolean[] activeImpacts = new boolean[savedImpactSettings.size()];
        int counter = 0;
        for (Map.Entry<Impact,Boolean> entry : savedImpactSettings.entrySet()) {
            impacts[counter] = entry.getKey().toString();
            boolean isActive = entry.getValue();
            activeImpacts[counter] = isActive;
            if (isActive) {
                selectedImpactSet.add(entry.getKey());
            }
            counter++;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select categories:");
        builder.setMultiChoiceItems(impacts, activeImpacts, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                Impact chosenImpact = Impact.valueOf(impacts[which]);
                if (isChecked) {
                    selectedImpactSet.add(chosenImpact);
                } else if (selectedImpactSet.contains(chosenImpact)) {
                    selectedImpactSet.remove(chosenImpact);
                }
            }
        });

        builder.setPositiveButton(Html.fromHtml("<b>Save</b>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Map<Impact,Boolean> map = new TreeMap<Impact,Boolean>();
                for (Map.Entry<Impact,Boolean> entry : savedImpactSettings.entrySet()) {
                    Impact tempImpact = entry.getKey();
                    map.put(tempImpact, selectedImpactSet.contains(tempImpact));
                }
                usp.updateImpactSettings(map);

                dialog.dismiss();
                ImpactChooserDialogListener activity;
                try {
                    activity = (ImpactChooserDialogListener)getActivity();
                    activity.onUpdateImpactChooser();
                } catch (ClassCastException e) {
                    // do nothing
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }

}
