package ch.si.forexeconomiccalendar.app.persistance;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import ch.si.forexeconomiccalendar.app.events.enums.Currency;
import ch.si.forexeconomiccalendar.app.events.enums.Impact;
import ch.si.forexeconomiccalendar.app.util.EventCategory;

/**
 * Created by kensteiner on 29/06/14.
 */
public class UserSettingsPersister {

    private final String EVENT_SETTINGS_FILE_KEY = "ch.si.forexeconomiccalendar.EVENT_SETTINGS";

    private final String HIGH_IMPACT = "EVENTS_SETTINGS_HIGH_IMPACT";
    private final String MEDIUM_IMPACT = "EVENTS_SETTINGS_MEDIUM_IMPACT";
    private final String LOW_IMPACT = "EVENTS_SETTINGS_LOW_IMPACT";

    private final String AUD = "EVENT_SETTINGS_AUD";
    private final String CAD = "EVENT_SETTINGS_CAD";
    private final String CHF = "EVENT_SETTINGS_CHF";
    private final String CNY = "EVENT_SETTINGS_CNY";
    private final String EUR = "EVENT_SETTINGS_EUR";
    private final String GBP = "EVENT_SETTINGS_GBP";
    private final String JPY = "EVENT_SETTINGS_JPY";
    private final String NZD = "EVENT_SETTINGS_NZD";
    private final String USD = "EVENT_SETTINGS_USD";

    private final String NOTIFICATION_ACTIVATED = "NOTIFICATION_ACTIVATED";
    private final String NOTIFICATION_TIME_SETTINGS = "NOTIFICATION_TIME_SETTINGS";

    private final String NOTIFICATION_SOUND_ACTIVATED = "NOTIFICATION_SOUND_ACTIVATED";
    private final String NOTIFICATION_SOUND_DEACTIVATED_UNTIL = "NOTIFICATION_SOUND_DEACTIVATED_UNTIL";

    private final String BOOT_RECEIVER_LAST_CALL = "BOOT_RECEIVER_LAST_CALL";

    private final String FIRST_APPLICATION_LAUNCH = "FIRST_APPLICATION_LAUNCH";

    private SharedPreferences sharedPreferences;

    public UserSettingsPersister(Context context){
        this.sharedPreferences = context.getSharedPreferences(EVENT_SETTINGS_FILE_KEY, Context.MODE_PRIVATE);
    }

    public Map<Impact,Boolean> readImpactSettings() {
        Map<Impact,Boolean> returnMap = new TreeMap<Impact, Boolean>();

        returnMap.put(Impact.LOW, sharedPreferences.getBoolean(LOW_IMPACT, true));
        returnMap.put(Impact.MEDIUM, sharedPreferences.getBoolean(MEDIUM_IMPACT, true));
        returnMap.put(Impact.HIGH, sharedPreferences.getBoolean(HIGH_IMPACT, true));

        return returnMap;
    }

    public Map<Currency,Boolean> readCurrencySettings() {
        Map<Currency,Boolean> map = new TreeMap<Currency, Boolean>();

        map.put(Currency.AUD, sharedPreferences.getBoolean(AUD, false));
        map.put(Currency.CAD, sharedPreferences.getBoolean(CAD, false));
        map.put(Currency.CHF, sharedPreferences.getBoolean(CHF, true));
        map.put(Currency.CNY, sharedPreferences.getBoolean(CNY, false));
        map.put(Currency.EUR, sharedPreferences.getBoolean(EUR, true));
        map.put(Currency.GBP, sharedPreferences.getBoolean(GBP, true));
        map.put(Currency.JPY, sharedPreferences.getBoolean(JPY, true));
        map.put(Currency.NZD, sharedPreferences.getBoolean(NZD, false));
        map.put(Currency.USD, sharedPreferences.getBoolean(USD, true));

        return map;
    }
    public void updateImpactSettings(Map<Impact,Boolean> inputMap) {
        if (inputMap.size() < 3) {
            Log.e("UserSettingsPersister", "wrong input");
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(HIGH_IMPACT, inputMap.get(Impact.HIGH));
        editor.putBoolean(MEDIUM_IMPACT, inputMap.get(Impact.MEDIUM));
        editor.putBoolean(LOW_IMPACT, inputMap.get(Impact.LOW));
        editor.commit();
    }

    public void updateCurrencySettings(Map<Currency,Boolean> inputMap) {
        if (inputMap.size() < 9) {
            Log.e("UserSettingsPersister", "wrong input");
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(AUD, inputMap.get(Currency.AUD));
        editor.putBoolean(CAD, inputMap.get(Currency.CAD));
        editor.putBoolean(CHF, inputMap.get(Currency.CHF));
        editor.putBoolean(CNY, inputMap.get(Currency.CNY));
        editor.putBoolean(EUR, inputMap.get(Currency.EUR));
        editor.putBoolean(GBP, inputMap.get(Currency.GBP));
        editor.putBoolean(JPY, inputMap.get(Currency.JPY));
        editor.putBoolean(NZD, inputMap.get(Currency.NZD));
        editor.putBoolean(USD, inputMap.get(Currency.USD));
        editor.commit();
    }

    public boolean readNotificationActivated() {
        return sharedPreferences.getBoolean(NOTIFICATION_ACTIVATED, false);
    }

    public void writeNotificationActivated(boolean state) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NOTIFICATION_ACTIVATED, state);
        editor.commit();
    }

    public int readNotificationTime() {
        return sharedPreferences.getInt(NOTIFICATION_TIME_SETTINGS, -5);
    }

    public void updateNotificationTime(int minutes) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(NOTIFICATION_TIME_SETTINGS, minutes);
        editor.commit();
    }

    public long readBootReceiverLastCall() {
        return sharedPreferences.getLong(BOOT_RECEIVER_LAST_CALL, 0);
    }

    public void updateBootReceiverLastCall() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(BOOT_RECEIVER_LAST_CALL, DateTime.now().getMillis());
        editor.commit();
    }

    public boolean readIsFirstLaunch() {
        boolean isFirstLaunch = sharedPreferences.getBoolean(FIRST_APPLICATION_LAUNCH, true);
        if (isFirstLaunch == true) {
            updateIsFirstLaunch();
        }
        return isFirstLaunch;
    }

    private void updateIsFirstLaunch() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(FIRST_APPLICATION_LAUNCH, false);
        editor.commit();
    }

    public boolean readNotificationSoundActivated() {
        return sharedPreferences.getBoolean(NOTIFICATION_SOUND_ACTIVATED, true);
    }

    public void updateNotificationSoundActivated(boolean active) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NOTIFICATION_SOUND_ACTIVATED, active);
        editor.commit();
    }

    public long readNotificationSoundDeactivatedUntil() {
        return sharedPreferences.getLong(NOTIFICATION_SOUND_DEACTIVATED_UNTIL, 0L);
    }

    public void updateNotificationSoundDeactivatedUntil(long time) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(NOTIFICATION_SOUND_DEACTIVATED_UNTIL, time);
        editor.commit();
    }

}