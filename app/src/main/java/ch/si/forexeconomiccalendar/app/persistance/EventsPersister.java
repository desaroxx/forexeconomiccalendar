package ch.si.forexeconomiccalendar.app.persistance;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.persistance.adapters.InternalStoragePersister;
import ch.si.forexeconomiccalendar.app.util.Week;

/**
 * Created by kensteiner on 29/06/14.
 */
public class EventsPersister<T> {
    final String EVENTS_LASTWEEK_STORAGE_FILENAME = "EELW";
    final String EVENTS_THISWEEK_STORAGE_FILENAME = "EETW";
    final String EVENTS_NEXTWEEK_STORAGE_FILENAME = "EENW";

    private Context context;

    public EventsPersister(Context context) {
        this.context = context;
    }

    public void storeEvents(List<T> eventList, Week week) {
        InternalStoragePersister.write(getFilename(week), eventList, context);
    }

    public List<T> retrieveStoredEvents(Week week) {
        List<T> retrievedList;
        try {
            retrievedList = (List<T>)InternalStoragePersister.read(getFilename(week), context);
        } catch (ClassCastException e) {
            retrievedList = new ArrayList<T>();
        }
        return retrievedList;
    }

    private String getFilename(Week week) {
        String STORAGE_FILENAME;
        switch (week) {
            case LAST_WEEK: STORAGE_FILENAME = EVENTS_LASTWEEK_STORAGE_FILENAME; break;
            case THIS_WEEK: STORAGE_FILENAME = EVENTS_THISWEEK_STORAGE_FILENAME; break;
            case NEXT_WEEK: STORAGE_FILENAME = EVENTS_NEXTWEEK_STORAGE_FILENAME; break;
            default: STORAGE_FILENAME = EVENTS_LASTWEEK_STORAGE_FILENAME; break;
        }
        return STORAGE_FILENAME;
    }

}
