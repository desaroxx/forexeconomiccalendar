package ch.si.forexeconomiccalendar.app.events.enums;

/**
 * Created by kensteiner on 17/07/14.
 */
public enum Impact {
    NON_ECONOMIC,
    LOW,
    MEDIUM,
    HIGH
}
