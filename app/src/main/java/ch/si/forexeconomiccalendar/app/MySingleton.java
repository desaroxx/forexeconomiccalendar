package ch.si.forexeconomiccalendar.app;

import android.util.Log;

/**
 * Created by kensteiner on 07/07/14.
 */
public class MySingleton {

    private static MySingleton instance;

    public String customVar;

    public static void initInstance() {
        if (instance == null) {
            instance = new MySingleton();
            Log.d("MySingleton", "new instance created");
        }
    }

    public static MySingleton getInstance() {
        if(instance == null) {
            initInstance();
        }
        return instance;
    }

    private MySingleton() {}
}
