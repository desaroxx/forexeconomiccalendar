package ch.si.forexeconomiccalendar.app.alarms;

import android.content.Context;
import android.media.SoundPool;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.events.FFProvider;
import ch.si.forexeconomiccalendar.app.events.entities.FFEvent;
import ch.si.forexeconomiccalendar.app.exceptions.NetworkDownException;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;
import ch.si.forexeconomiccalendar.app.persistance.NotificationsPersister;
import ch.si.forexeconomiccalendar.app.util.Week;

/**
 * Created by kensteiner on 01/07/14.
 */
public class NotificationsManager {

    private Context context;
    private SoundPool sp;

    public NotificationsManager(Context context) {
        this.context = context;
    }

    public void updateNotifications() {
        // notification everytime sync is called
        playSound();

        // make sure syncAlarm is set
        new LocalNotificationsAlarmReceiver().resetSyncAlarm(context);

        // abort if sync is inactive
        if(!isSyncActive()) {
            removeAll();
            return;
        }

        FFProvider ffProvider = new FFProvider(context);

        List<FFEvent> events = new ArrayList<FFEvent>();
        // get events from this and next week
        try {
            events = ffProvider.provideAllFromWeek(Week.THIS_WEEK);
            events.addAll(ffProvider.provideAllFromWeek(Week.NEXT_WEEK));
        } catch (NetworkDownException e) {
            events = ffProvider.provideAllFromWeekWithoutUpdate(Week.THIS_WEEK);
            events.addAll(ffProvider.provideAllFromWeekWithoutUpdate(Week.NEXT_WEEK));
        }
        if(events.size() == 0) {
            Log.e("NotificationsManager", "not adding any events to be notified");
        }
        add(events);
    }

    public void createNotifications() {

        playSoundTwice();
        // make sure syncAlarm is set
        new LocalNotificationsAlarmReceiver().setSyncAlarm(context);

        // abort if sync is inactive
        if(!isSyncActive()) {
            removeAll();
            return;
        }

        FFProvider econodayProvider = new FFProvider(context);

        List<FFEvent> events = new ArrayList<FFEvent>();
        // get events from this and next week
        try {
            events = econodayProvider.provideAllFromWeek(Week.THIS_WEEK);
            events.addAll(econodayProvider.provideAllFromWeek(Week.NEXT_WEEK));
        } catch (NetworkDownException e) {
            events = econodayProvider.provideAllFromWeekWithoutUpdate(Week.THIS_WEEK);
            events.addAll(econodayProvider.provideAllFromWeekWithoutUpdate(Week.NEXT_WEEK));
        }
        if(events.size() == 0) {
            Log.e("NotificationsManager", "not adding any events to be notified");
        }
        add(events);
    }

    public void add(List<FFEvent> events) {
        removeAll();

        LocalNotificationsAlarmReceiver lnar = new LocalNotificationsAlarmReceiver();

        List<NotificationEntity> notificationEntityList = new ArrayList<NotificationEntity>();

        // add new alarms
        Log.d("NotificationsManager", "setting " + events.size() + " alarms (past events will be stripped");
        for(FFEvent ee : events) {
            notificationEntityList.add(new NotificationEntity(ee.getTime(), ee.getTitle()));

        }
        lnar.setAlarms(context, notificationEntityList);

    }

    public void removeAll() {
        LocalNotificationsAlarmReceiver lnar = new LocalNotificationsAlarmReceiver();

        // delete all from peristance
        NotificationsPersister notificationsPersister = new NotificationsPersister(context);
        List<NotificationEntity> notificationEntityList = notificationsPersister.removeAll();

        // cancel all alarms (pendingIntents)
        lnar.cancelAlarms(context, notificationEntityList);
    }

    public void printQueuedNotifications() {
        List<NotificationEntity> notificationEntityList = new NotificationsPersister(context).retrieve();
        Log.d("NotificationsManager", "queued notifications count: " + notificationEntityList.size());
        for(NotificationEntity ne : notificationEntityList) {
            Log.d("NotificationsManager", "queued notification: " + ne.getEventTime().toString() + " - " + ne.getTitle() + " (Hash: " + ne.getUuid().hashCode() + ")");
        }
    }

    private void playSound() {
//        sp = new SoundPool(2, AudioManager.STREAM_MUSIC, 100);
//        sp.load(context, R.raw.ffs, 1);
//        sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
//            @Override
//            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
//                sp.play(1, 1f, 1f, 1, 0, 1f);
//            }
//        });
    }

    private void playSoundTwice() {
//        sp = new SoundPool(2, AudioManager.STREAM_MUSIC, 100);
//        sp.load(context, R.raw.cff, 1);
//        sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
//            @Override
//            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
//                sp.play(1, 1f, 1f, 1, 0, 1f);
//            }
//        });
    }

    private boolean isSyncActive() {
        return new UserSettingsPersister(context).readNotificationActivated();
    }
}
