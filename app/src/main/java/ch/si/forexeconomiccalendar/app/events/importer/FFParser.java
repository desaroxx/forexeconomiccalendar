package ch.si.forexeconomiccalendar.app.events.importer;

import android.util.Log;

import org.joda.time.DateTime;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import ch.si.forexeconomiccalendar.app.events.entities.DayEntity;
import ch.si.forexeconomiccalendar.app.events.entities.FFEvent;
import ch.si.forexeconomiccalendar.app.events.entities.PerformanceEntity;
import ch.si.forexeconomiccalendar.app.events.entities.TimeEntity;
import ch.si.forexeconomiccalendar.app.events.enums.*;


/**
 * Created by kensteiner on 17/07/14.
 */
public class FFParser {

    private Document inputDocument = null;
    private long belongsToWeekId = 0l;

    private List<FFEvent> ffEventList;

    private String CALENDER_ROW_SELECTOR = ".calendar_row";

    public FFParser(Document inputDocument, long belongsToWeekId) {
        this.inputDocument = inputDocument;
        this.belongsToWeekId = belongsToWeekId;

        long startTime = System.currentTimeMillis();
        extractEvents();
        long duration = System.currentTimeMillis() - startTime;
        Log.d("FFParser", "list extraction duration in ms: " + duration);
//        for (FFEvent e : ffEventList) {
//            Log.d("FFParser", "Created: " + e.getTime() + " " + e.getTitle() + ", " + e.getImpact() + ", " + e.getCurrency() + ", " + e.getActual().getValue() + ", " + e.getForecast().getValue()+ ", " + e.getPrevious().getValue());
//        }
    }

    public List<FFEvent> getFFEvents() { return ffEventList; }

    private void extractEvents() {

        ffEventList = new ArrayList<FFEvent>();
        Elements elements = inputDocument.select(CALENDER_ROW_SELECTOR);

        int id;
        int month = 0;
        int day = 0;
        Weekday weekday = Weekday.SUNDAY;
        DateTime time = new DateTime();
        Currency currency;
        Impact impact;
        String title;
        PerformanceEntity actual;
        PerformanceEntity forecast;
        PerformanceEntity previous;

        for (Element element : elements) {
            // check if new day --> update when necessary
            if (isNewDay(element)) {
                DayEntity dayEntitiy = getDayEntitiy(element);
                weekday = dayEntitiy.getWeekday();
                month = dayEntitiy.getMonth();
                day = dayEntitiy.getDay();
            }

            // move to next if no event
            if (!hasEvent(element)) {
                continue;
            }

            // move to next if eventcategory non-economic event

            impact = getImpact(element);
            if (impact == Impact.NON_ECONOMIC) {
                continue;
            }

            // valid time format
            if (!hasValidTimeFormat(element)) {
                continue;
            }

            id = getId(element);

            // check if has time
            if (hasTime(element)) {
                TimeEntity te = getTimeEntity(element);
                time = calculateEventTime(weekday, te.getHour(), te.getMinutes());
            }

            currency = getCurrency(element);
            title = getTitle(element);
            actual = getPerformance(element, "actual");
            forecast = getPerformance(element, "forecast");
            previous = getPerformance(element, "previous");

            //Log.d("FFParser", "create FFEvent: " + time + " " + title);
            ffEventList.add(new FFEvent(id, time, title, impact, currency, actual, forecast, previous));
        }
    }


    private boolean hasEvent(Element element) {
        Elements elements = element.select("tr[data-eventid~=[0-9]+]");
        return elements.size() != 0;
    }

    private boolean isNewDay(Element element) {
        Elements elements = element.select("span.date");
        if (elements.size() > 0) {
            return true;
        }
        return false;
    }

    private DayEntity getDayEntitiy(Element element) {
        String extractedString = "";

        String weekdayString = "";
        Elements elements = element.select("span.date");
        if (elements.size() > 0) {
            extractedString = elements.first().text();
            weekdayString = extractedString.substring(0,3);
        }
        Weekday weekday;
        if (weekdayString.equals("Sun")) {
            weekday = Weekday.SUNDAY;
        } else if (weekdayString.equals("Mon")) {
            weekday = Weekday.MONDAY;
        } else if (weekdayString.equals("Tue")) {
            weekday = Weekday.TUESDAY;
        } else if (weekdayString.equals("Wed")) {
            weekday = Weekday.WEDNESDAY;
        } else if (weekdayString.equals("Thu")) {
            weekday = Weekday.THURSDAY;
        } else if (weekdayString.equals("Fri")) {
            weekday = Weekday.FRIDAY;
        } else {
            weekday = Weekday.SATURDAY;
        }

        String monthString = extractedString.substring(3,6);
        int month = 0;
        if (monthString.equals("Jan")) {
            month = 1;
        } else if (monthString.equals("Feb")) {
            month = 2;
        } else if (monthString.equals("Mar")) {
            month = 3;
        } else if (monthString.equals("Apr")) {
            month = 4;
        } else if (monthString.equals("May")) {
            month = 5;
        } else if (monthString.equals("Jun")) {
            month = 6;
        } else if (monthString.equals("Jul")) {
            month = 7;
        } else if (monthString.equals("Aug")) {
            month = 8;
        } else if (monthString.equals("Sep")) {
            month = 9;
        } else if (monthString.equals("Oct")) {
            month = 10;
        } else if (monthString.equals("Nov")) {
            month = 11;
        } else {
            month = 12;
        }

        String dayString = extractedString.substring(7);
        int day = Integer.parseInt(dayString);

        return new DayEntity(month, day, weekday);
    }

    private boolean hasId(Element element) {
        Element selectedElement = element.select("tr.calendar_row").first();
        String id = selectedElement.attr("data-eventid");
        if (id.length() < 4) {
            return false;
        }
        return true;
    }

    private int getId(Element element) {
        Element selectedElement = element.select("tr.calendar_row").first();
        String id = selectedElement.attr("data-eventid");
        return Integer.parseInt(id);
    }


    private boolean hasValidTimeFormat(Element element) {
        Element tdTimeElement = element.select("td.time").first();
        Log.d("FFParser", "tdTimeElement: " + getTitle(element) + " " + tdTimeElement);

        // check if is upcoming event
        if (tdTimeElement.children().size() >= 2) {
            // handle: is upcoming event
            tdTimeElement.child(0).remove();
            Log.d("FFParser", "tdTimeElement "  + (getTitle(element).length()!=0 ? getTitle(element): "not found") + " (adjusted): " + tdTimeElement);
        }
        String time = tdTimeElement.text();
        System.out.println("Time: " + time);
        return (time.length() == 0) || (isValidTimeFormat(time));
    }

    private boolean hasTime(Element element) {
        Element tdTimeElement = element.select("td.time").first();
        Log.d("FFParser", "tdTimeElement: " + tdTimeElement);

        // check if is upcoming event
        if (tdTimeElement.children().size() >= 2) {
            // handle: is upcoming event
            Log.d("FFParser", "childcount: " + tdTimeElement.children().size());
            tdTimeElement.child(0).remove();
            Log.d("FFParser", "tdTimeElement (adjusted): " + tdTimeElement);
        }
        String time = tdTimeElement.text();
        return isValidTimeFormat(time);
    }

    private boolean isValidTimeFormat(String inputString) {
        if (inputString.length() > 7) {
            System.out.println("too long (inputString.length() = " + inputString.length());
            return false;
        }

        if (inputString.length() == 0) {
            System.out.println("0 length");
            return false;
        }

        char firstChar = inputString.charAt(0);
        if (!(firstChar >= '0' && firstChar <= '9')) {
            System.out.println("first char no numeric: " + firstChar);
            return false;
        }

        String ampm = inputString.substring(inputString.length()-2);
        if (!(ampm.equals("am") || ampm.equals("pm")) ) {
            System.out.println("ampmfailed: " + ampm);
            return false;
        }

        String time = inputString.substring(0, inputString.length()-2);
        if (time.charAt(time.length()-3) != ':') {
            System.out.println("doublepoint not found: " + time.charAt(time.length()-3));
            return false;
        }

        return true;
    }

    private TimeEntity getTimeEntity(Element element) {
        String inputString = element.select("td.time").text();
        String hourString = inputString.substring(0, inputString.length() - 5);
        String minuteString = inputString.substring(inputString.length()-4, inputString.length()-2);
        String ampmString = inputString.substring(inputString.length()-2);

        System.out.println("extractedstring: " + hourString + "h " + minuteString + "m " + ampmString + "...");

        int hour = Integer.parseInt(hourString);
        int minute = Integer.parseInt(minuteString);
        boolean isAm = ampmString.equals("am");
        if (isAm && (hour==12)) {
            hour = 0;
        }
        if (!isAm && (hour != 12)) {
            hour += 12;
        }

        return new TimeEntity(hour, minute);
    }

    private Currency getCurrency(Element element) {
        Currency returnCurrency = Currency.AUD;
        String currencyString = element.select("td.currency").text();
        if (currencyString.equals("AUD")) {
            returnCurrency = Currency.AUD;
        } else if (currencyString.equals("CAD")) {
            returnCurrency = Currency.CAD;
        } else if (currencyString.equals("CHF")) {
            returnCurrency = Currency.CHF;
        } else if (currencyString.equals("CNY")) {
            returnCurrency = Currency.CNY;
        } else if (currencyString.equals("EUR")) {
            returnCurrency = Currency.EUR;
        } else if (currencyString.equals("GBP")) {
            returnCurrency = Currency.GBP;
        } else if (currencyString.equals("JPY")) {
            returnCurrency = Currency.JPY;
        } else if (currencyString.equals("NZD")) {
            returnCurrency = Currency.NZD;
        } else if (currencyString.equals("USD")) {
            returnCurrency = Currency.USD;
        }
        return returnCurrency;
    }

    private Impact getImpact(Element element) {
        Impact returnImpact = Impact.NON_ECONOMIC;
        Elements elements = element.select("td.impact > div > span");

        if (elements.size() < 1) {
            return Impact.NON_ECONOMIC;
        }

        Element impactElement = elements.first();

        String title = impactElement.attr("title").toLowerCase().substring(0, 3);

        if (title.equals("hig")) {
            return Impact.HIGH;
        } else if (title.equals("med")) {
            return Impact.MEDIUM;
        } else if (title.equals("low")) {
            return Impact.LOW;
        }
        return Impact.NON_ECONOMIC;
    }

    private String getTitle(Element element) {
        return element.select("td.event").text();
    }

    private PerformanceEntity getPerformance(Element element, String selector) {
        Performance performance;
        String value = "";
        Elements betterElements = element.select("td." + selector + " > span.better");

        if (betterElements.size() > 0) {
            performance = Performance.BETTER;
            value = betterElements.first().text();
            return new PerformanceEntity(value, performance);
        }

        Elements worseElements = element.select("td." + selector + " > span.worse");
        if (worseElements.size() > 0) {
            performance = Performance.WORSE;
            value = worseElements.first().text();
            return new PerformanceEntity(value, performance);
        }

        Elements expectedElements = element.select("td." + selector);
        performance = Performance.EXPECTED;
        value = expectedElements.first().text();
        return new PerformanceEntity(value, performance);
    }

    private DateTime calculateEventTime(Weekday weekday, int hour, int minute) {
        DateTime solutionTime = new DateTime(belongsToWeekId);
        solutionTime = solutionTime.plusMinutes(minute);
        solutionTime = solutionTime.plusHours(hour);
        switch (weekday) {
            case SUNDAY:
                break;
            case MONDAY:
                solutionTime = solutionTime.plusDays(1);
                break;
            case TUESDAY:
                solutionTime = solutionTime.plusDays(2);
                break;
            case WEDNESDAY:
                solutionTime = solutionTime.plusDays(3);
                break;
            case THURSDAY:
                solutionTime = solutionTime.plusDays(4);
                break;
            case FRIDAY:
                solutionTime = solutionTime.plusDays(5);
                break;
            case SATURDAY:
                solutionTime = solutionTime.plusDays(6);
                break;
        }
        return solutionTime;
    }
}
