package ch.si.forexeconomiccalendar.app.events.entities;

import ch.si.forexeconomiccalendar.app.events.enums.Weekday;

/**
 * Created by kensteiner on 18/07/14.
 */
public class DayEntity {
    private int month;
    private int day;
    private Weekday weekday;

    public DayEntity(int month, int day, Weekday weekday) {
        this.month = month;
        this.day = day;
        this.weekday = weekday;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public Weekday getWeekday() {
        return weekday;
    }
}
