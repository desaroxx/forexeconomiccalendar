package ch.si.forexeconomiccalendar.app.exceptions;

/**
 * Created by kensteiner on 07/07/14.
 */
public class EmptyEventsException extends Exception {
    public EmptyEventsException() {
        super();
    }

    public EmptyEventsException(String message) {
        super(message);
    }

}