package ch.si.forexeconomiccalendar.app.lists;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.events.entities.FFEvent;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by kensteiner on 31/05/14.
 */
public class CustomWithNextArrayAdapter extends ArrayAdapter<FFEvent> implements StickyListHeadersAdapter {

    private LayoutInflater inflater;
    private Context context;
    private FFEvent[] events;
    private int nextEventPosition;

    public CustomWithNextArrayAdapter(Context context, int resource, FFEvent[] objects) {
        super(context, resource, objects);
        Log.d("CustomArrayAdapter", "init");
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.events = objects;
    }

    /* private view holder class */
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtPerformance;
        TextView txtTime;
        TextView txtCurrency;
        TextView txtNext;
    }

    private class HeaderViewHolder {
        TextView txtMenuName;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        FFEvent rowItem = getItem(position);
        int type = getItemViewType(position);
        Log.d("CustomArrayAdapter", "getView: " + position + ", type: " + type + ", " + convertView );

        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            switch(type) {
                case 0:
                    convertView = inflater.inflate(R.layout.custom_content_row, parent, false);

                    holder.imageView = (ImageView) convertView.findViewById(R.id.status_color);
                    holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
                    holder.txtPerformance = (TextView) convertView.findViewById(R.id.source);
                    holder.txtTime = (TextView) convertView.findViewById(R.id.time);
                    holder.txtCurrency = (TextView) convertView.findViewById(R.id.time_cst);
                    break;
                case 1:
                    convertView = inflater.inflate(R.layout.custom_content_row_next, parent, false);

                    holder.imageView = (ImageView) convertView.findViewById(R.id.status_color_next);
                    holder.txtTitle = (TextView) convertView.findViewById(R.id.title_next);
                    holder.txtPerformance = (TextView) convertView.findViewById(R.id.source_next);
                    holder.txtTime = (TextView) convertView.findViewById(R.id.time_next);
                    holder.txtCurrency = (TextView) convertView.findViewById(R.id.time_cst_next);
                    holder.txtNext = (TextView) convertView.findViewById(R.id.next_next);
                    break;
            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imageView.setImageResource(R.drawable.rectangle_neutral);
        switch (rowItem.getImpact()) {
            case HIGH: holder.imageView.setImageResource(R.drawable.rectangle_red); break;
            case MEDIUM: holder.imageView.setImageResource(R.drawable.rectangle_orange); break;
            case LOW: holder.imageView.setImageResource(R.drawable.rectangle_yellow); break;
            default: holder.imageView.setImageResource(R.drawable.rectangle_neutral); break;
        }

        holder.txtTitle.setText(rowItem.getTitle());

        DateTime eventTime = rowItem.getTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
        String eventTimeLocalFormatted = fmt.withZone(DateTimeZone.getDefault()).print(eventTime);
        holder.txtTime.setText(eventTimeLocalFormatted);

        holder.txtCurrency.setText(rowItem.getCurrency().toString());

        String actual = rowItem.getActual().getValue();
        if (actual.length() < 1) {
            actual = "---";
        }
        String forecast = rowItem.getForecast().getValue();
        if (forecast.length() < 1) {
            forecast = "---";
        }
        String previous = rowItem.getPrevious().getValue();
        if (previous.length() < 1) {
            previous = "---";
        }

        holder.txtPerformance.setText("Act: " + actual + "   Fore: " + forecast + "   Prev: " + previous);

        if(type == 1) {
            holder.txtNext.setText("Next: in " + timeTillEvent(position));
        }

        return convertView;

    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        FFEvent rowItem = getItem(position);

        HeaderViewHolder holder;
        if(convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.custom_header_row, parent, false);

            holder.txtMenuName = (TextView) convertView.findViewById(R.id.header_name);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        String formattedDate = rowItem.getTime().toDateTime(DateTimeZone.getDefault()).toString(fmt);
        int dayOfWeek = rowItem.getTime().getDayOfWeek();

        String dayName = "";
        switch (dayOfWeek) {
            case 0: dayName = "Sunday"; break;
            case 1: dayName = "Monday"; break;
            case 2: dayName = "Tuesday"; break;
            case 3: dayName = "Wednesday"; break;
            case 4: dayName = "Thursday"; break;
            case 5: dayName = "Friday"; break;
            case 6: dayName = "Saturday"; break;
            case 7: dayName = "Sunday"; break;
        }
        holder.txtMenuName.setText(dayName + ", " + formattedDate);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMddyyyy");
        DateTime time = events[position].getTime().toDateTime(DateTimeZone.getDefault());
        Log.d("CustomWithNextArrayAdapter", "getHeaderId pos: " + position + " | returns " + new Long(time.toString(fmt)));
        return 100*new Long(time.toString(fmt));
    }

    @Override
    public int getItemViewType(int position) {
        DateTime timeNow = DateTime.now(DateTimeZone.getDefault());
        DateTime nextEventTime = getItem(position).getTime().toDateTime(DateTimeZone.getDefault());

        boolean isNext = false;
        if(position == 0) {
            if(timeNow.getMillis() < nextEventTime.getMillis()) {
                isNext = true;
                nextEventPosition = position;
            }
        } else {
            DateTime lastEventTime = getItem(position-1).getTime().toDateTime(DateTimeZone.getDefault());
            if((timeNow.getMillis() < nextEventTime.getMillis()) && (timeNow.getMillis() > lastEventTime.getMillis())) {
                isNext = true;
                nextEventPosition = position;
            }
        }
        if(isNext) {
            Log.d("CustomWithNextArrayAdapter", "getItemViewType pos: " + position + " | returns 1");
            return 1;
        }
        Log.d("CustomWithNextArrayAdapter", "getItemViewType pos: " + position + " | returns 0");
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    private String timeTillEvent(int position){
        DateTime timeNow = DateTime.now(DateTimeZone.getDefault());
        DateTime eventTime = getItem(position).getTime().toDateTime(DateTimeZone.getDefault());

        Period period = new Period(timeNow, eventTime);
        PeriodFormatter DayFormatter = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(1)
                .appendDays()
                .toFormatter();
        PeriodFormatter HourFormatter = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(1)
                .appendHours()
                .toFormatter();
        PeriodFormatter MinutesFormatter = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(1)
                .appendMinutes()
                .toFormatter();

        String returnString = "";

        String days = DayFormatter.print(period) + " d ";
        String hours = HourFormatter.print(period) + " h ";
        String minutes = MinutesFormatter.print(period) + " min";

        if(!days.equals("0 d ")) {
            returnString += days;
        }
        if(!hours.equals("0 h ")) {
            returnString += hours;
        }
        returnString += minutes;

        return returnString;
    }

    public int getNextEventPosition() {
        long timeNow = DateTime.now(DateTimeZone.getDefault()).getMillis();

        for(int position = 0; position < events.length; position++) {
            long eventTime = getItem(position).getTime().toDateTime(DateTimeZone.getDefault()).getMillis();

            if(position == 0) {
                if(timeNow < eventTime) {
                    return position;
                }
            } else {
                long lastEventTime = getItem(position-1).getTime().toDateTime(DateTimeZone.getDefault()).getMillis();
                if((timeNow < eventTime) && (timeNow > lastEventTime)) {
                    return position;
                }
            }
        }
        return 0;
    }

    public FFEvent getEvent(int position) {
        return events[position];
    }

}