package ch.si.forexeconomiccalendar.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.List;

import ch.si.forexeconomiccalendar.app.GlobalApplication;
import ch.si.forexeconomiccalendar.app.activities.EventDetailsActivity;
import ch.si.forexeconomiccalendar.app.events.FFProvider;
import ch.si.forexeconomiccalendar.app.events.entities.FFEvent;
import ch.si.forexeconomiccalendar.app.events.importer.ImportManager;
import ch.si.forexeconomiccalendar.app.exceptions.NetworkDownException;
import ch.si.forexeconomiccalendar.app.lists.CustomArrayAdapter;
import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.lists.CustomWithNextArrayAdapter;
import ch.si.forexeconomiccalendar.app.util.Week;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by kensteiner on 24/06/14.
 */
public class EventsSectionFragment extends Fragment {

    private Week week;

    protected FFProvider econodayProvider;

//    private DialogFragment networkProblemDialog;
//    private int dialogFragmentCounter = 0;
    protected View rootView;
    protected Context context;

    private LinearLayout linlaHeaderProgress;
    private LinearLayout linlaNoData;

    public EventsSectionFragment() {}

    private ImportManager importManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sticky_list_headers_listview, container, false);

        // set context
        context = getActivity().getApplicationContext();

        // set week
        String targetWeek = getArguments().getString("TARGET_WEEK");
        week = parseWeek(targetWeek);

        // set content handlers
        importManager = new ImportManager(context);
        econodayProvider = new FFProvider(context);

        setHasOptionsMenu(true);
        Log.d("EventsSectionFragment", "setHasOptionsMenu");

        // analytics tracking
        trackPage();

        // load page
        loadPage();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.d("EventsSectionFragment","onCreateOptionsMenu");
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public void onResume() {
        super.onResume();

        // reload page
        loadPage();

//        dialogFragmentCounter = 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        Log.d("EventsSectionFragment", "onOptionsItemSelected " + item);
        int id = item.getItemId();
        if (id == R.id.refresh) {
            reloadData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private void loadPage() {
        if(importManager.needsImport(week)) {
            Log.d("EventsSectionFragment", "Up to date data not available. Importing...");
            new ProvideEventsAsyncTask().execute(week);
        } else {
            // TODO: mysteriously falls into this category
            Log.d("EventsSectionFragment", "Data is still good. no import");
            List<FFEvent> economicEventList = econodayProvider.provideAllFromWeekWithoutUpdate(week);
            updateArrayAdapter(week, economicEventList);
        }
    }

    private void reloadData() {
        Log.d("EventsSectionFragment", "reloadData");
        new RefreshEventsAsyncTask().execute(week);
    }

    protected class ProvideEventsAsyncTask extends AsyncTask<Week, Integer, FFEvent[]> {

        private boolean showNoData = false;
        @Override
        protected void onPreExecute() {
            showLoading();
        }

        @Override
        protected FFEvent[] doInBackground(Week... params) {
            week = params[0];

            List<FFEvent> economicEventList = null;
            try {
                economicEventList = econodayProvider.provideAllFromWeek(week);
            } catch (NetworkDownException e) {
//                loadNetworkProblemDialog();
                economicEventList = econodayProvider.provideAllFromWeekWithoutUpdate(week);
                if (economicEventList.size() == 0) {
                    showNoData = true;
                }
            }
            FFEvent[] economicEvents = new FFEvent[economicEventList.size()];
            return economicEventList.toArray(economicEvents);
        }

        @Override
        protected void onPostExecute(FFEvent[] result) {
            dismissLoading();
            if (showNoData) {
                Log.d("EventSectionFragment", "onPostExecute path showNoData");
                showNoData();
                return;
            }
            updateArrayAdapter(week, result);
            new UpdateEventsAsyncTask().execute();
        }

    }

    protected class UpdateEventsAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                econodayProvider.refresh();
            } catch (NetworkDownException e) {
                Log.e("EventSectionFragment", "refresh failed --> network connectivity");
            }
            return null;
        }
    }

    protected class RefreshEventsAsyncTask extends AsyncTask<Week, Integer, FFEvent[]> {

        private boolean showNoData = false;
        @Override
        protected void onPreExecute() {
            showLoading();
        }

        @Override
        protected FFEvent[] doInBackground(Week... params) {
            week = params[0];

            List<FFEvent> economicEventList = null;
            try {
                economicEventList = econodayProvider.provideAllFromWeekForceUpdate(week);
            } catch (NetworkDownException e) {
                showNoData = true;
                return null;
            }
            FFEvent[] economicEvents = new FFEvent[economicEventList.size()];
            return economicEventList.toArray(economicEvents);
        }

        @Override
        protected void onPostExecute(FFEvent[] result) {
            dismissLoading();
            if (showNoData) {
                showNoData();
                return;
            }
            updateArrayAdapter(week, result);
        }

    }

    protected void updateArrayAdapter(Week week, FFEvent[] economicEvents) {
        StickyListHeadersListView stickyList = (StickyListHeadersListView) rootView.findViewById(R.id.list);
        //stickyList.setPadding(0,66,0,0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(0, 66, 0, 0);
        stickyList.setLayoutParams(layoutParams);

        switch(week) {
            case THIS_WEEK:
                final CustomWithNextArrayAdapter adapterWithNext = new CustomWithNextArrayAdapter(getActivity(), R.id.list, economicEvents);
                stickyList.setAdapter(adapterWithNext);

                int selection = adapterWithNext.getNextEventPosition();
                stickyList.setSelection(selection);

                stickyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        FFEvent fee = adapterWithNext.getEvent(position);
                        int eventId = fee.getId();
                        long time = fee.getTime().getMillis();
                        String impact = fee.getImpact().toString();
                        String title = fee.getTitle();

                        Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
                        intent.putExtra("id", eventId);
                        intent.putExtra("time", time);
                        intent.putExtra("title", title);
                        intent.putExtra("impact", impact);
                        getActivity().startActivity(intent);

                    }
                });
                return;
            default:
                final CustomArrayAdapter adapter = new CustomArrayAdapter(getActivity(), R.id.list, economicEvents);
                stickyList.setAdapter(adapter);

                stickyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                });

                stickyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        FFEvent fee = adapter.getEvent(position);
                        int eventId = fee.getId();
                        long time = fee.getTime().getMillis();
                        String impact = fee.getImpact().toString();
                        String title = fee.getTitle();

                        Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
                        intent.putExtra("id", eventId);
                        intent.putExtra("time", time);
                        intent.putExtra("impact", impact);
                        intent.putExtra("title", title);
                        getActivity().startActivity(intent);

                    }
                });
                return;
        }
    }

    protected void updateArrayAdapter(Week week, List<FFEvent> economicEventList) {
        FFEvent[] economicEvents = castListToArray(economicEventList);
        updateArrayAdapter(week, economicEvents);
    }

    protected FFEvent[] castListToArray(List<FFEvent> economicEventList) {
        FFEvent[] economicEvents = new FFEvent[economicEventList.size()];
        return economicEventList.toArray(economicEvents);
    }

//    private void loadNetworkProblemDialog() {
//        if (dialogFragmentCounter > 0 ) {
//            return;
//        }
//        networkProblemDialog = NetworkProblemDialogFragment.newInstance();
//        networkProblemDialog.show(getActivity().getSupportFragmentManager(), "");
//        ++dialogFragmentCounter;
//    }

    private Week parseWeek(String inputString) {
        Week selectedWeek;
        if(inputString.equals("LAST_WEEK")) {
            selectedWeek = Week.LAST_WEEK;
        } else if (inputString.equals("THIS_WEEK")) {
            selectedWeek = Week.THIS_WEEK;
        } else {
            // NEXT WEEK
            selectedWeek = Week.NEXT_WEEK;
        }
        return selectedWeek;
    }

    private String getScreenName() {
        switch (week) {
            case LAST_WEEK:
                return "ch.si.forexeconomiccalendar.EventsSectionFragment.LAST_WEEK";
            case THIS_WEEK:
                return "ch.si.forexeconomiccalendar.EventsSectionFragment.THIS_WEEK";
            case NEXT_WEEK:
                return "ch.si.forexeconomiccalendar.EventsSectionFragment.NEXT_WEEK";
        }
        return null;
    }

    private void trackPage() {
        Tracker t = ((GlobalApplication)getActivity().getApplication()).getTracker(GlobalApplication.TrackerName.APP_TRACKER);
        t.setScreenName(getScreenName());
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void showLoading() {
        Log.d("EventsSectionFragment" , "show: loading");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                linlaHeaderProgress = (LinearLayout) rootView.findViewById(R.id.linlaHeaderProgress);
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        });
    }

    private void dismissLoading() {
        Log.d("EventsSectionFragment" , "dismiss: loading");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("EventsSectionFragment", "linlaHeaderProgress: " + linlaHeaderProgress);
                if (linlaHeaderProgress != null) {
                    linlaHeaderProgress.setVisibility(View.GONE);
                }
            }
        });

    }

    private void showNoData() {
        Log.d("EventsSectionFragment" , "show: no data");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                linlaNoData = (LinearLayout) rootView.findViewById(R.id.linlaNoData);
                linlaNoData.setVisibility(View.VISIBLE);

                Button retryButton = (Button) linlaNoData.findViewById(R.id.retry_button);
                retryButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        retry();
                    }
                });
            }
        });
    }

    private void dismissNoData() {
        Log.d("EventsSectionFragment", "dismissing: no data");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                linlaNoData = (LinearLayout) rootView.findViewById(R.id.linlaNoData);
                linlaNoData.setVisibility(View.GONE);
            }
        });
    }

    private void retry() {
        dismissNoData();
        new ProvideEventsAsyncTask().execute(week);
    }
}
