package ch.si.forexeconomiccalendar.app.util;

/**
 * Created by kensteiner on 29/06/14.
 */
public enum Week {
    LAST_WEEK,
    THIS_WEEK,
    NEXT_WEEK
}
