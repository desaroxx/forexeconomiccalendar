package ch.si.forexeconomiccalendar.app.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.List;

import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;
import ch.si.forexeconomiccalendar.app.persistance.NotificationsPersister;

/**
 * Created by kensteiner on 30/06/14.
 */
public class BootReceiver extends BroadcastReceiver {
    LocalNotificationsAlarmReceiver localNotificationsAlarmReceiver = new LocalNotificationsAlarmReceiver();

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED") || intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")) {
            Log.d("BootReceiver", "reloading alarms");
            UserSettingsPersister esp = new UserSettingsPersister(context);
            esp.updateBootReceiverLastCall();

            localNotificationsAlarmReceiver.setSyncAlarm(context);

            NotificationsPersister notificationsPersister = new NotificationsPersister(context);
            List<NotificationEntity> notificationEntityList = notificationsPersister.removeAll();
            localNotificationsAlarmReceiver.setAlarms(context, notificationEntityList);
        }
    }
}
