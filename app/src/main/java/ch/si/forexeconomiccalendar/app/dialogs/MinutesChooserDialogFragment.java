package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import ch.si.forexeconomiccalendar.app.R;
import ch.si.forexeconomiccalendar.app.persistance.UserSettingsPersister;

/**
 * Created by kensteiner on 02/07/14.
 */
public class MinutesChooserDialogFragment extends DialogFragment {
    private UserSettingsPersister usp;
    private NumberPicker hoursPicker;
    private NumberPicker minutesPicker;
    private RadioGroup radioGroup;
    private RadioButton radioButtonAfter;
    private RadioButton radioButtonBefore;

    private int time;
    private int hours;
    private int minutes;
    private boolean before;

    public static MinutesChooserDialogFragment newInstance() {
        MinutesChooserDialogFragment frag = new MinutesChooserDialogFragment();
        return frag;
    }

    public MinutesChooserDialogFragment() {}

    public interface MinutesChooserDialogListener {
        public void update();

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        usp = new UserSettingsPersister(getActivity().getApplicationContext());
        time = usp.readNotificationTime();

        hours = calculateHours(time);
        minutes = calculateMinutes(time);
        before = calculateIsBefore(time);


        LayoutInflater inflater = getActivity().getLayoutInflater();

        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.dialog_minutes_chooser, null);

        hoursPicker = (NumberPicker) linearLayout.findViewById(R.id.hoursPicker);
        hoursPicker.setMinValue(0);
        hoursPicker.setMaxValue(23);
        hoursPicker.setFocusable(true);
        hoursPicker.setFocusableInTouchMode(true);
        hoursPicker.setValue(hours);

        minutesPicker = (NumberPicker) linearLayout.findViewById(R.id.minutesPicker);
        minutesPicker.setMinValue(0);
        minutesPicker.setMaxValue(59);
        minutesPicker.setFocusable(true);
        minutesPicker.setFocusableInTouchMode(true);
        minutesPicker.setValue(minutes);

        radioGroup = (RadioGroup) linearLayout.findViewById(R.id.radio_group);

        radioButtonBefore = (RadioButton) linearLayout.findViewById(R.id.radio_before);
        radioButtonAfter = (RadioButton) linearLayout.findViewById(R.id.radio_after);

        if (before) {
            radioButtonBefore.setChecked(true);
        } else {
            radioButtonAfter.setChecked(true);
        }

        builder.setView(linearLayout);

        builder.setTitle("Set Time of Reminder");

        builder.setPositiveButton(Html.fromHtml("<b>Save</b>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int flipper = (radioButtonBefore.isChecked()) ? -1 : 1;
                int totalTime = flipper * ((hoursPicker.getValue()*60) + minutesPicker.getValue());
                usp.updateNotificationTime(totalTime);
                dialog.dismiss();

                MinutesChooserDialogListener activity;
                try {
                    activity = (MinutesChooserDialogListener) getActivity();
                    activity.update();
                } catch (ClassCastException e) {
                    // do nothing
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    private int calculateMinutes(int time) {
        return (time > 0) ? (time % 60) : ((-1 * time) % 60);
    }

    private int calculateHours(int time) {
        return (time > 0) ? (time / 60) : ((-1 * time) / 60);
    }

    private boolean calculateIsBefore(int time) {
        return time <= 0;
    }

    private int calculateTime(int hours, int minutes, boolean before) {
        int inverter = -1;
        if (!before) {
            inverter = 1;
        }
        return inverter * ((60 * hours) + minutes);
    }
}
