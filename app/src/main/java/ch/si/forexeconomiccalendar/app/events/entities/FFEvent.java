package ch.si.forexeconomiccalendar.app.events.entities;

import org.joda.time.DateTime;

import java.io.Serializable;

import ch.si.forexeconomiccalendar.app.events.enums.Currency;
import ch.si.forexeconomiccalendar.app.events.enums.Impact;

/**
 * Created by kensteiner on 17/07/14.
 */
public class FFEvent implements Serializable{
    private int id;
    private DateTime time;
    private String title;
    private Impact impact;
    private Currency currency;
    private PerformanceEntity actual;
    private PerformanceEntity forecast;
    private PerformanceEntity previous;

    public FFEvent(int id, DateTime time, String title, Impact impact, Currency currency, PerformanceEntity actual, PerformanceEntity forecast, PerformanceEntity previous) {
        this.id = id;
        this.time = time;
        this.title = title;
        this.impact = impact;
        this.currency = currency;
        this.actual = actual;
        this.forecast = forecast;
        this.previous = previous;
    }

    public int getId() {
        return id;
    }

    public DateTime getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public Impact getImpact() {
        return impact;
    }

    public Currency getCurrency() {
        return currency;
    }

    public PerformanceEntity getActual() {
        return actual;
    }

    public PerformanceEntity getForecast() {
        return forecast;
    }

    public PerformanceEntity getPrevious() {
        return previous;
    }
}
