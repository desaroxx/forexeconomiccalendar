package ch.si.forexeconomiccalendar.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by kensteiner on 07/07/14.
 */
public class NetworkProblemDialogFragment extends DialogFragment {
    public static NetworkProblemDialogFragment newInstance() {
        NetworkProblemDialogFragment frag = new NetworkProblemDialogFragment();
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle("Can't access internet!")
                .setMessage("Make sure your internet is on.")
                .setPositiveButton("Got it!",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .create();
    }
}
