package ch.si.forexeconomiccalendar.app.events.entities;

import java.io.Serializable;

/**
 * Created by kensteiner on 19/07/14.
 */
public class FFHistoryEntry implements Serializable {
    private String date;
    private String actual;
    private String forecast;
    private String previous;

    public FFHistoryEntry(String date, String actual, String forecast, String previous) {
        this.date = date;
        this.actual = actual;
        this.forecast = forecast;
        this.previous = previous;
    }

    public String getTitle() {
        return date;
    }

    public String getActual() {
        return actual;
    }

    public String getForecast() {
        return forecast;
    }

    public String getPrevious() {
        return previous;
    }
}
